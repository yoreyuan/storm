## 1 安装
### 1.1 版本问题
版本选择 1.x -> 2.x -> 5.x

[1]: max file descriptors [4096] for elasticsearch process is too low, increase to at least [65536]
	vi /etc/security/limits.conf
	* soft nofile 65536
	* hard nofile 131072
	* soft nproc 2048
	* hard nproc 4096
[2]: max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]
	vi /etc/sysctl.conf 
	vm.max_map_count=655360

npm run start


http.cors.enabled: true
http.cors.allow-origin: "*"

cluster.name: 集群名
node.name: 节点名
node.master: true

从节点
discovery.zen.ping.unicast.hosts: ["node1"]

#### 重启服务
ps -ef | grep `pwd`

----

## 2 基本概念
索引：含有相同属性的文档集合(databases)
类型：索引可以定义一个或多个类型，文档必须属于一个类型(table)
文档：文档是可以被索引的基本数据单位（row）

分片：每个索引都是多个分片，每个分片是一个Lucene索引
备份：拷贝一份分片就完成了分片的备份

## 3 RESTFul API
基本格式 http://ip:port/索引/类型/文档
常用HTTP动词： GET、PUT、POST、DELETE

(1)结构化的索引
POST	book/novel/_mappings
{
	"novel":{
		"properties":{
			"title":{"type":"text"}
		}
	}
}

PUT	ip:9200/people
{
	"settings":{
		"number_of_shards": 1,
		"number_of_replicas": 1
	},
	"mappings":{
		"house":{
			"dynamic": false,
			"properties":{
				"name":{
					"type":"string"
				},
				"country":{
					"type":"string"
				},
				"age":{
					"type":"integer"
				},
				"date":{
					"type":"date",
					"format":"yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
				}
			}
		}
	}
}


插入数据(指定ID)
PUT		ip:9200/people/man/1
{
	"name":"yore",
	"country":"China",
	"age":"20",
	"date":"2018-12-03"
}
插入数据(自动ID)
POST		ip:9200/people/man
{
	"name":"Yuan",
	"country":"China",
	"age":"21",
	"date":"2018-12-03"
}

修改数据(直接修改)
POST		ip:9200/people/man/1/_update
{
	"doc":{
		"name":"my name is yore"
	}
}
修改数据(脚本修改)
PUT		ip:9200/people/man/1/_update
{
	"script":{
		"lang":"painless",
		"inline":"ctx._source.age = params.age",
		"params":{
			"age":22
		}
	}
}

删除数据
DELETE		ip:9200/people/man/1
删除索引
DELETE		ip:9200/people

(2)常用查询
添加数据
PUT	book
{
	"settings":{
		"number_of_shards": 3,
		"number_of_replicas": 1
	},
	"mappings":{
		"novel":{
			"properties":{
				"word_count":{"type":"integer"},
				"author":{"type":"keyword"},
				"title":{"type":"text"},
				"publish_date":{
					"type":"date",
					"format":"yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
				}
			}
		}
	}
}
简单查询

条件查询

聚合查询

POST	http://i-hbm8wksw:9200/boot/house/_search
{
	"query":{
		"match":{
			"name":"李芯蕊"
		}
	}
}

中文分词
medcl/elasticsearch-analysis-ik
解压到plugins下

"analyzer": "ik_max_word" / "ik_smart"
"search_analyzer":"ik_max_word"

_analyzer?analyzer=ik_max_word&pretty=true&value=文书



