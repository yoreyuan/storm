
Elasticsearch
---

## 一、环境和版本
Elasticsearch版本 5.6.2
Lucene_version版本    5.5.0

Elasticsearch service `http://i-hbm8wksw:9200/`
Elasticsearch Head  `http://i-hbm8wksw:9100/`

## 一、对外接口
### 1、常用基本造作
1.1 创建索引
PUT   http://i-hbm8wksw:9200/索引名?pretty
PUT   http://i-hbm8wksw:9200/索引名/type/索引id
如果返回如下标识创建成功
```
{
    "acknowledged": true
}
```  

1.2 查询库的状态    http://i-hbm8wksw:9200/_cat/indices?v

1.3 插入数据
http://i-hbm8wksw:9200/索引名/type
_id保留，不能使用这个字段
```
put http://i-hbm8wksw:9200/yore/cpws/2/

{
    "id" : "9f73da3ec2fbbc0c",
    "cpws_json" : [
        {
            "doc_group" : "92BC7691FB172B23",
            "doc_type" : "1",
            "judge_result" : "<p>驳回上诉，维持原判。<p>二审案件受理费452元，由上诉人抚顺罕王兴洲矿业有限公司负担。<p>本判决为终审判决。",
        }
    ],
    "load_time" : "2018-10-17T08:30:35.810Z"
}
```
1.4 修改文档
修改完后的状态json中可以看到create=false，标识数据是修改的
```
put   http://i-hbm8wksw:9200/yore/cpws/2
{
    "id" : "9f73da3ec2fbbc0c",
    "cpws_json" : [
        {
            "doc_group" : "92BC7691FB172B23",
            "doc_type" : "2",
            "judge_result" : "<p>驳回上诉，维持原判。<p>二审案件受理费452元，由上诉人抚顺罕王兴洲矿业有限公司负担。<p>本判决为终审判决。",
        }
    ],
    "load_time" : "2018-10-17T08:30:35.810Z"
}
```
1.5 查询数据
```
GET   http://i-hbm8wksw:9200/yore/cpws/2
```

1.5 删除数据
```
DELETE   http://i-hbm8wksw:9200/yore/cpws/2
```

1.6 删除索引
head --> 概览 --> 索引 --> 动作 --> 删除
```
DELETE   http://i-hbm8wksw:9200/yore/cpws/2
```

### 2.Java API
 POST 新增，
 PUT 更新，
 GET 获取，
 DELETE 删除，
 HEAD 判断是否存在

引入依赖是有两种
org.elasticsearch.client » transport
org.elasticsearch.client » rest


## 索引
索引是具有相同结构的文档集合。

对于一个文档，会包含一个或多个字段，任何字段都有自己的数据类型，Elasticsearch是通过映射来进行字段和数据类型对应的。
```
##创建索引
put     http://ip:9200/索引名/
参数：
{
    "settings" : {"number_of_shards": 3, "number_of_replicas": 2}
}
其中默认的分片数为5，副本数为1

##更新索引
put http://ip:9200/索引名/_settings/
参数：
{
    "number_of_replicas": 2
}

##删除索引
delete  http://ip:9200/索引名/
可以使用_all或*号删除全部索引
为防止误删，可以设置elasticsearch.yml属性action.destructive_requires_name=true，进制使用通配符删除索引

##获取索引
get http://ip:9200/索引名/
获取索引，可使用通配符_all或*获取全部索引。

##打开或关闭索引
关闭的索引只能显示源数据信息，不能够进行读写操作。
post    http://ip:9200/索引名/_open
post    http://ip:9200/索引名/_close

```
索引映射管理
```
##添加索引，并设置文档类型为log，其中字段message类型文string
put http://ip:9200/索引名/
参数{
    "mapping": {
        "log":{
            "properties":{
                "message":{"type": "string"}
            }
        }
    }
}
##向已存在的索引添加文档类型文user，包含name，字段类型是string
put http://ip:9200/索引名/_mapping/user
参数{
    "properties" {
        "name" : {"type": "string"}
    }
}

```

在同一个索引的不同type中，相同的名称的字段中必须有相同的映射
```
例如修改映射的类型
put http://ip:9200/索引名/_mapping/type名?update_all_types
参数{
    "text": {
        "type": "string",
        "analyzer": "standard",
        "search_analyzer": "whitespace"
    }
}

##获取映射
get http://ip:9200/索引名/_mapping/type名

##获取字段映射
get http://ip:9200/索引名/_mapping/type名/field/字段名
```
索引别名(类似于数据库中的视图)
ES可以对一个或多个索引指定别名，通过别名可以查询到一个或多个索引的内容。
在系统中索引是不能重复的，也不可以和索引重名。
别名不能进行修改，可以先删除原别名，在新增别名。
```
##添加别名
post    http://ip:9200/_aliases
参数{
    "actions" : [{
        "add" : {"index": "secisland", "alias": "aliasl"}
    }]
}

##删除别名
参数{
    "actions" : [{
        "remove" : {"index": "secisland", "alias": "aliasl"}
    }]
}

##过滤索引别名
put     http://ip:9200/索引名/
参数{}

##删除别名
delete  http://ip:9200/索引名/_alias/名字

##查询所有别名
get http://ip:9200/索引名/_alias/*
```

索引分析
analysis：首先会把一个文本快分析称一个个单独的词(term)，为了后面的倒排索引做准备，
然后标准化这些词为标准形式，提高他们的"可搜索行"。这些工作是分词器(analyzers)完成的。
一个分词器是一个组合，用于将三个功能放到一起。
字段过滤器：
   字符串经过字段过滤器（character Filter）处理，他们的工作是在标记化之前处理字符串。
   字符过滤器能够去除HTML标记，或者转换为"&"为"and".
分词器：
   分词器（tokenizer）被标记化成独立的次。一个简单的分词器可以根据空格或逗号将单词分开。
标记过滤器：
   每个次都通过所有标记过滤处理，它可以修改词，去除词，或者增加词
   
ES提供很多内置的字符过滤器、分词器和标记过滤器。这些可以组合创建自定的分词器以应对不同的需求。
```
##标准分词器，会将text拆分成单个词：
post   http://i-hbm8wksw:9200/_analyze
{
    "analyzer": "standard",
    "text": "this is a test"
}

##过滤html字符，同时转小写
{
    "tokenizer": "keyword",
    "token_filters": ["lowercase"],
    "char_filters": ["html_strip"],
    "text": "this is a <b>test<b>"
}

{
    "tokenizer": "standard",
    "token_filters": ["snowball"],
    "text": "detail output",
    "explain" : true,
    "attributes": ["keyword"]
    
}

PUT m_cpws_info
{
    "settings": {
        "analysis": {
            "analyzer": {"my_analyzer": {
                "tokenizer": "my_tokenizer"
                }
            },
            "tokenizer": {
                "my_tokenizer": {
                    "type": "standard",
                    "max_token_length": 5
                }
            }
        }
    }
}

```

索引常用请求
```
##查看索引状态
get http://i-hbm8wksw:9200/m_cpws_info/_stats

##索引分片
get     http://i-hbm8wksw:9200/m_cpws_info/_segments

##刷新索引
get     http://i-hbm8wksw:9200/m_cpws_info/_refresh

##flush接口数据
post    http://i-hbm8wksw:9200/m_cpws_info/_flush/

##完全启用脚本 config/elasticsearch.yml
添加如下配置，然后从起Elasticsearch
script.inline: on
script.indexed: on
script.file: on

统计索引次信息
post http://i-hbm8wksw:9200/posts/doc/1/_termvectors?pretty=true
{
    "fields" : ["message"],
    "offsets": true,
    "payloads": true,
    "positions": true,
    "term_statistics":true,
    "field_statistics":true
}

```





















































