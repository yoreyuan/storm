package com.yore;

import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.support.ActiveShardCount;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.action.support.replication.ReplicationResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.get.GetResult;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.junit.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by yore on 2018/11/16 5:33 PM
 */
public class UpdateAPI {

    /**
     *
     * Update API
     * 更新
     *
     * Updata API允许通过使用脚本或传递部分文档来更新现有文档
     *
     * @date: 2018/11/16 5:33 PM
     */
    @Test
    public void part4() throws IOException {
        RestHighLevelClient client = EsHighApi.getRestHighLevelClient();

        // index,type,Document id
        UpdateRequest request = new UpdateRequest(
                "posts",
                "doc",
                "1"
        );


        /** 使用脚本更新 */
        // 脚本参数以一个Map对象提供
        Map<String, Object> parameters = Collections.singletonMap("count", 4);
        // 使用painless语言创建内联脚本，并传入参数值
        Script inline = new Script(ScriptType.INLINE, "painless",
                "ctx._source.file += params.count", parameters);
        request.script(inline);

        /** 或者是一个存储脚本：*/
        Script stored = new Script(ScriptType.STORED, "painless", "increment-field", parameters);
        request.script(stored);


        /**
         * 使用局部文档更新
         *
         * 使用局部文档更新时，局部文档将与现有文档合并
         */
        String jsonString = "{" +
                "\"updated\" : \"2018-11-18\"," +
                "\"reason\" : \"daily update\"" +
                "}";
        // 以一个JSON格式的字符串提供的局部文档源
        request.doc(jsonString, XContentType.JSON);

        // 以一个可以自动转换为JSON格斯的Map提供局部文档源
        Map<String, Object> jsonMap = new HashMap<>();
        jsonMap.put("updated", "2018-11-18");
        jsonMap.put("reason", "daily update");
        request.doc(jsonMap);

        // 以XContentBuilder对象提供局部文档源，ES构建辅助器将生成JSON格式
        XContentBuilder builder = XContentFactory.jsonBuilder();
        builder.startObject();
        {
            builder.field("updated", "2018-11-18");
            builder.field("reason", "daily update");
        }
        builder.endObject();
        request.doc(builder);

        // 以Object键值对提供局部文档源，它将被转换为JSON格式
        request.doc(
                "updated", new Date(),
                "reason", "daily update"
        );


        /**
         * 更新或插入
         *
         * 如果温度囊不存在，可以使用upsert方法定义一些将文档作为新文档插入的内容。
         *
         * 与局部文档更新类似，可以使用接收String、Map、XContentBuilder或Object键值对的方式使用upsert方法更新或插入文档的内容
         */
        jsonString = "{\"created\" : \"2018-11-18\"}";
        // 以字符串提供更新插入的文档源
        request.upsert(jsonString, XContentType.JSON);


        /**
         * 可选参数
         */
        // 路由值
        request.routing("routing");

        // Parent值
        request.parent("parent");

        // TimeValue类型的等待朱分片可用的超时时间
        request.timeout(TimeValue.timeValueMillis(2));
        //request.timeout("2m");

        request.setRefreshPolicy(WriteRequest.RefreshPolicy.WAIT_UNTIL);
        //request.setRefreshPolicy("wait_for");

        request.retryOnConflict(3);

        // Enable source retrieval ,disabled by default
        request.fetchSource(true);

        request.version(2);

        // Disable the noop detection
        request.detectNoop(false);

        request.scriptedUpsert(true);

        request.docAsUpsert(true);

        request.waitForActiveShards(ActiveShardCount.ALL);
        //request.waitForActiveShards(2);


        // 配置数据源包含的特殊字段
        String[] includes = new String[]{"updated", "r*"};
        String[] excludes = Strings.EMPTY_ARRAY;
        request.fetchSource(new FetchSourceContext(true, includes, excludes));


        // 配置源不包含的字段
        includes = Strings.EMPTY_ARRAY;
        excludes = new String[]{"updated"};
        request.fetchSource(new FetchSourceContext(true, includes, excludes));


        /** 同步执行 */
        UpdateResponse updateResponse = client.update(request);
        /*
        * 当对一个不存在的文档UpdateRequest时，响应将包含404状态吗，
        * 并抛出一个需要如下处理的ElasticsearchException异常
         */
        try {
            updateResponse = client.update(request);
        }catch (ElasticsearchException e){
            if(e.status() == RestStatus.NOT_FOUND){
                // 处理由于文档不存在导致的异常
            }
        }
        /*
        * 如果有文档冲突，也会抛出一个异常
        * 并抛出一个需要如下处理的ElasticsearchException异常
         */
        try {
            request.version(1);
            updateResponse = client.update(request);
        }catch (ElasticsearchException e){
            if(e.status() == RestStatus.CONFLICT){
                // 表名异常是由于返回来了版本冲突错误导致的
            }
        }


        /** 异步执行 */
        client.updateAsync(request, new ActionListener<UpdateResponse>() {
            @Override
            public void onResponse(UpdateResponse updateResponse) {
                //当执行成功时回调，响应对象以参数的形式传入。
            }

            @Override
            public void onFailure(Exception e) {
                //  失败时被是被调动。异常对象以参数的形式传入
            }
        });


        /**
         * 更新响应
         *
         * 返回的UpdateResponse允许获取执行操作的相关信息，如下所示
         */
        String index = updateResponse.getIndex();
        String type = updateResponse.getType();
        String id = updateResponse.getId();
        long version = updateResponse.getVersion();
        if(updateResponse.getResult() == DocWriteResponse.Result.CREATED){
            // 处理当文档是第一次创建（upsert）
        }else if(updateResponse.getResult() == DocWriteResponse.Result.UPDATED){
            // 处理当文档是被updated
        }else if(updateResponse.getResult() == DocWriteResponse.Result.NOOP){
            // no operation
        }

        /*
        * 通过fetchsource方法在UpdateRequest里设置了启用连接源，响应独享将包含被更新的文档源
        */
        // 获取更新文档作为一个GetResult
        GetResult result = updateResponse.getGetResult();
        if(result.isExists()){
            // 获取更新的文档源作为String
            String sourceAsString = result.sourceAsString();
            Map<String, Object> sourceAsMap = result.sourceAsMap();
            byte[] sourceAsBytes = result.source();
        }else{
            // 处理序列化，当源文档在响应对象中是不存在的
        }


        // 也可以用检查分片故障
        ReplicationResponse.ShardInfo shardInfo = updateResponse.getShardInfo();
        if(shardInfo.getTotal() != shardInfo.getSuccessful()){
            // 处理当成功的分片数小于总分片数时的情况
        }
        if(shardInfo.getFailed() > 0){
            for(ReplicationResponse.ShardInfo.Failure failure : shardInfo.getFailures()){
                // 处理潜在的失败
                String reason = failure.reason();
            }
        }

    }

}
