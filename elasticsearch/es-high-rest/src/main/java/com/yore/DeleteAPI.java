package com.yore;

import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.action.support.replication.ReplicationResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.VersionType;
import org.elasticsearch.rest.RestStatus;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by yore on  2018/11/16 4:29 PM
 */
public class DeleteAPI {

    /**
     * Delete API
     *
     */
    @Test
    public void part3() throws IOException {
        RestHighLevelClient client = EsHighApi.getRestHighLevelClient();

        // index,type,Document id
        DeleteRequest request = new DeleteRequest(
                "posts",
                "doc",
                "1"
        );

        // 路由值
        request.routing("routing");

        // Parent值
        request.parent("parent");

        // TimeValue类型的等待主分片可用的超时时间
        request.timeout(TimeValue.timeValueMillis(2));
        //request.timeout("2m");

        // 刷新策略
        request.setRefreshPolicy(WriteRequest.RefreshPolicy.WAIT_UNTIL);
        //request.setRefreshPolicy("wait_for");

        // Verison
        request.version(2);

        // Verison Type
        request.versionType(VersionType.EXTERNAL);


        /** 同步执行 */
        DeleteResponse deleteResponse = client.delete(request);


        /** 异步执行 */
        client.deleteAsync(request, new ActionListener<DeleteResponse>() {
            @Override
            public void onResponse(DeleteResponse deleteResponse) {
                // 当执行成功完成时被调，响应对象以参数的形式传入。
            }

            @Override
            public void onFailure(Exception e) {
                // 失败时被是被调动。异常对象以参数的形式传入
            }
        });


        /** delete操作的响应 */
        String index = deleteResponse.getIndex();
        String type = deleteResponse.getType();
        String id = deleteResponse.getId();
        long verison = deleteResponse.getVersion();

        ReplicationResponse.ShardInfo shardInfo = deleteResponse.getShardInfo();
        if(shardInfo.getTotal() != shardInfo.getSuccessful()){
            // 当成功的片数小于总片数的情况
        }
        if(shardInfo.getFailed() > 0){
            for(ReplicationResponse.ShardInfo.Failure failure : shardInfo.getFailures()){
                // 处理潜在的故障
                String reason = failure.reason();
            }
        }

        request = new DeleteRequest("posts", "doc", "does_not_exist");
        deleteResponse = client.delete(request);
        if(deleteResponse.getResult() == DocWriteResponse.Result.NOT_FOUND){
            // 如果被删除的文档没有被找到，执行某些操作
        }


        try{
            request = new DeleteRequest("posts", "doc", "1").version(2);
            deleteResponse = client.delete(request);
        }catch (ElasticsearchException e){
            if(e.status() == RestStatus.CONFLICT){
                // 由于版本冲突错误导致的异常
            }
        }

    }

}
