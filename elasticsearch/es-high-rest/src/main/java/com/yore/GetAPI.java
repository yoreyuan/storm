package com.yore;

import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.Strings;
import org.elasticsearch.index.VersionType;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;

/**
 * Created by yore on 2018/11/16 3:29 PM
 */
public class GetAPI {

    /**
     * Get API
     *
     * @auther: yore
     * @date: 2018/11/16 3:29 PM
     */
    @Test
    public void part2() throws IOException {
        RestHighLevelClient client = EsHighApi.getRestHighLevelClient();

        // index,type,Document id
        GetRequest request = new GetRequest(
                "posts",
                "doc",
                "1"
        );

        // 禁用检索源，默认为启动
        request.fetchSourceContext(new FetchSourceContext(false));

        /*String[] includes = new String[]{"message", "Date"};
        String[] excludes = Strings.EMPTY_ARRAY;
        FetchSourceContext fetchSourceContext = new FetchSourceContext(true, includes, excludes);
        //设置源包含的特定域
        request.fetchSourceContext(fetchSourceContext);*/

        String[] includes = Strings.EMPTY_ARRAY;
        String[] excludes = new String[]{"message"};
        FetchSourceContext fetchSourceContext = new FetchSourceContext(true, includes, excludes);
        // 配置数据源不包含的特定域
        request.fetchSourceContext(fetchSourceContext);

        // 配置检索的特定存储域
        request.storedFields("message");

        /** 同步执行 */
        GetResponse getResponse = client.get(request);
        /**
         * 当针对不存在的索引执行获取请求时，应用有404状态码，抛出一个ElasticsearchException
         *
         * 如果请求了特定文档版本，但现有文档具有不同的版本号，request.version(2)
         *      则也会抛出ElasticsearchException，引起版本冲突
         *
         */
        try {
            getResponse = client.get(request);
        }catch (ElasticsearchException e){
            if(e.status() == RestStatus.NOT_FOUND){
                // 处理因为索引不存在而抛出的异常
            }

            if(e.status() == RestStatus.CONFLICT){
                // 标识版本有冲突
            }
        }

        // 获取检索的message存储域
        String message = (String) getResponse.getField("message").getValue();

        // Routing value
        request.routing("routing");

        // Rarent value
        request.parent("parent");

        // Preference value
        request.preference("preference");

        // set realtime a refresh before retrieving the document。默认为false
        request.refresh(true);

        // version
        request.version(2);

        //Version type
        request.versionType(VersionType.EXTERNAL);


        /** 异步执行 */
        client.getAsync(request, new ActionListener<GetResponse>() {
            @Override
            public void onResponse(GetResponse getFields) {
                // 当执行成功完成时被调，响应对象以参数的形式传入。
            }

            @Override
            public void onFailure(Exception e) {
                // 故障时被是被调动。异常对象以参数的形式传入
            }
        });


        /** 获取响应 */
        String index = getResponse.getIndex();
        String type = getResponse.getType();
        String id = getResponse.getId();
        if(getResponse.isExists()){
            long version = getResponse.getVersion();
            // retrieve 文档字符串
            String sourceAsString = getResponse.getSourceAsString();
            Map<String, Object> sourceAsMap = getResponse.getSourceAsMap();
            byte[] sourceAsBytes = getResponse.getSourceAsBytes();
        }else{
            /*
            * Handle(处理)当文档没有找到的情况（scenario）。
            * 注意尽管获取的响应是404的状态码，一个有效的GetResponse被返回，而不是一个异常抛出
            */
        }

    }

}
