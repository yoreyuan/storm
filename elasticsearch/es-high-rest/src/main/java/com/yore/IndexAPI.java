package com.yore;

import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.action.support.replication.ReplicationResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.VersionType;
import org.elasticsearch.rest.RestStatus;
import org.junit.Test;

import java.io.IOException;
import java.util.Date;

/**
 * Created by yore on 2018/11/15 3:27
 */
public class IndexAPI {

    /**
     * Index
     *
     * @auther: yore
     * @date: 2018/11/15 3:27 PM
     */
    @Test
    public void part1() throws IOException {
        RestHighLevelClient client = EsHighApi.getRestHighLevelClient();

        // index,type,Document id
        IndexRequest request = new IndexRequest(
                "posts",
                "doc",
                "1"
        );

        /*String jsonString = "{" +
                "\"user\": \"yore\", " +
                "\"postDate\": \"2018-11-15\", " +
                "\"message\": \"trying out Elasticsearch\" " +
                "}";
        // 以字符串提供的Document source
        request.source(jsonString, XContentType.JSON);*/


        /** 以Map形式 */
        /*Map<String, Object> jsonMap = new HashMap<>();
        jsonMap.put("user", "yore");
        jsonMap.put("postDate", new Date());
        jsonMap.put("messate", "trying out Elasticsearch");
        request.source(jsonMap);*/


        /** 以XContentBuilder形式构建一个josn对象 */
        XContentBuilder builder = XContentFactory.jsonBuilder();
        builder.startObject();
        {
            builder.field("user", "yore");
            builder.field("postDate", new Date());
            builder.field("messate", "trying out Elasticsearch");
        }
        builder.endObject();
        request.source(builder);


        /** 以键值对对象作为文档来源，它自动转换为JSON格式 */
        /*request.source("user", "yore", "postDate", new Date(), "messate", "trying out Elasticsearch");*/


        // Routing值, 路由
        request.routing("routing");

        // Parent值
        request.parent("parent");

        // 等待主分片可用的超时时间
        request.timeout(TimeValue.timeValueSeconds(1));
        //request.timeout("1s");

        // 以WriteRequest.RefreshPolicy实例的刷新策略参数
        request.setRefreshPolicy(WriteRequest.RefreshPolicy.WAIT_UNTIL);
        //request.setRefreshPolicy("wait_for");

        // 版本
        request.version(2);
        // 版本类型
        request.versionType(VersionType.EXTERNAL);

        // 提供一个DocWriteRequest.OpType值作为操作类型
        request.opType(DocWriteRequest.OpType.CREATE);
        //request.opType("create");

        //在索引文档之前要执行的摄取管道的名称
        request.setPipeline("pipeline");


        /** 同步执行 */
        IndexResponse indexResponse = client.index(request);

        /**
         * 如果存在版本冲突： request.version(1)
         *      会抛出ElasticsearchException
         *
         * 如果存在版本冲突： request.toType(DocWriteRequest.OpType.CREATE)
         *      会抛出ElasticsearchException
         */
        try{
            indexResponse = client.index(request);
        }catch (ElasticsearchException e){

            if(e.status() == RestStatus.CONFLICT){
                // 表示是由于返回了版本冲突错误引起的异常

                // 或者在设置类型时，如果冲突了也会引起次异常
            }
        }


        /** 异步执行 */
        client.indexAsync(request, new ActionListener<IndexResponse>() {
            @Override
            public void onResponse(IndexResponse indexResponse) {
                // 当操作成功完成是被调用。响应对象以参数的形式传入。
            }

            @Override
            public void onFailure(Exception e) {
                // 故障时被是被调动。异常对象以参数的形式传入
            }
        });


        /** Index响应 */
        String index = indexResponse.getIndex();
        String type = indexResponse.getType();
        String id = indexResponse.getId();
        long version = indexResponse.getVersion();
        if(indexResponse.getResult() == DocWriteResponse.Result.CREATED){
            // 处理（如果需要）首次创建文档的情况
        }else if(indexResponse.getResult() == DocWriteResponse.Result.UPDATED){
            // 处理（如果需要）文档已经存在时被覆盖的情况
        }
        ReplicationResponse.ShardInfo shardInfo = indexResponse.getShardInfo();
        if(shardInfo.getTotal() != shardInfo.getSuccessful()){
            // 处理成功的碎片数少于总碎片的情况
        }
        if(shardInfo.getFailed() > 0){
            for(ReplicationResponse.ShardInfo.Failure failure : shardInfo.getFailures()){
                // 处理潜在的故障
                String reason = failure.reason();
            }
        }

    }

}
