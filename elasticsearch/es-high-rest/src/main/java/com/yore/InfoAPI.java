package com.yore;

import org.elasticsearch.Build;
import org.elasticsearch.Version;
import org.elasticsearch.action.main.MainResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.cluster.ClusterName;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by yore on 2018/11/19 11:42
 */
public class InfoAPI {

    /**
     * Info API
     *
     * @date: 2018/11/19 11:45 AM
     */
    @Test
    public void part7() throws IOException {
        RestHighLevelClient client = EsHighApi.getRestHighLevelClient();

        MainResponse response = client.info();

        /**
         * 响应
         */
        // 获取包含集合名称信息的ClusterName对象
        ClusterName clusterName = response.getClusterName();

        // 获取集群的唯一标识符
        String clusterUuid = response.getClusterUuid();

        // 获取执行请求的节点名称
        String nodeName = response.getNodeName();

        // 获取已执行请求的节点版本
        Version version = response.getVersion();

        // 获取已执行请求的节点的构建信息
        Build build = response.getBuild();

    }

}
