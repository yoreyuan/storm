package com.yore;

import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.search.*;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by yore on 2018/11/19 10:26
 */
public class SearchScrollAPI {

    @Test
    public void part7() throws IOException {
        RestHighLevelClient client = EsHighApi.getRestHighLevelClient();

        /**
         * 初始化滚动上下文
         */
        SearchRequest searchRequest = new SearchRequest("posts");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        MatchQueryBuilder matchQuery = new MatchQueryBuilder("title", "Elasticsearch");
        searchSourceBuilder.query(matchQuery);
        searchSourceBuilder.size(5);

        searchRequest.source(searchSourceBuilder);
        // 设置滚动间隔
        searchRequest.scroll(TimeValue.timeValueMinutes(1L));

        /**
         * 同步执行
         */
        SearchResponse searchResponse = client.search(searchRequest);

        // 读取一个返回的Scroll id，
        String scrollId = searchResponse.getScrollId();
        SearchHits hits = searchResponse.getHits();


        /**
         * 检索所有相关文档
         *
         * 其次，接收到的滚动标识符必须被设置到下一个新的滚动间隔的SearchScrollRequest，并通过searchScroll方法发送。
         * Elasticsearch会使用新的滚动标识符返回另一批结果。然后可以在随后的SearchScrollRequest中使用此新的滚动标识符来检索下一批结果，等等。
         * 应该循环重复此过程，知道不再返回结果，这意味着滚动已经用尽，并且检索到所有匹配的文档。
         */
        SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
        scrollRequest.scroll(TimeValue.timeValueSeconds(30));
        SearchResponse searchScrollResponse = client.searchScroll(scrollRequest);
        scrollId = searchScrollResponse.getScrollId();
        hits = searchScrollResponse.getHits();

        Assert.assertEquals(3, hits.getTotalHits());
        Assert.assertEquals(1, hits.getHits().length);
        Assert.assertNotNull(scrollId);


        /**
         * 清除滚动上下文
         *
         * 最后可以使用Clear Scroll API删除最后一个滚动标识符，以释放搜索上下文。
         * 当文档到期时会自动发生，但最佳实战是当滚动回话结束后尽快释放资源。
         */


        /**
         * 可选参数
         */
        scrollRequest.scroll(TimeValue.timeValueSeconds(60L));
        //scrollRequest.scroll("60s");

        /**
         * 异步执行
         */
        client.searchScrollAsync(scrollRequest, new ActionListener<SearchResponse>() {
            @Override
            public void onResponse(SearchResponse searchResponse) {
                // 当执行成功时被调用，响应对象以参数形式传入
            }

            @Override
            public void onFailure(Exception e) {
                // 失败是调用，异常以参数形式传入
            }
        });

        /**
         * 响应
         *
         * 与Search API一样，滚动搜索API也返回一个SearchResponse对象
         */


        /**
         * 一个完整示例
         */
        // 滚动间隔是1分钟
        final Scroll scroll = new Scroll(TimeValue.timeValueMinutes(1L));
        searchRequest = new SearchRequest("posts");
        searchRequest.scroll(scroll);
        searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(matchQuery);
        // 通过发送初始化SearchRequest来初始化检索上下文
        searchResponse = client.search(searchRequest);

        scrollId = searchResponse.getScrollId();
        SearchHit[] searchHits = searchResponse.getHits().getHits();
        // 在一个循环中通过调用Search Scroll API检索所有命中结果，直到没有文档返回为止
        while (searchHits != null && searchHits.length>0){
            scrollRequest = new SearchScrollRequest(scrollId);
            scrollRequest.scroll(scroll);
            scrollId = searchResponse.getScrollId();
            // 处理返回的搜索结果
            searchHits = searchResponse.getHits().getHits();
        }


        /**
         * Clear Scroll API
         * 使用search Scroll API的搜索上下文在超过滚动时间时，会自动清除。
         * 但是，建议当不再需要使用Clear Scroll API后尽可能快释放搜索上下文。
         */
        // 一旦滚动完成，清除滚动上下文
        ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
        clearScrollRequest.addScrollId(scrollId);
        ClearScrollResponse clearScrollResponse = client.clearScroll(clearScrollRequest);
        boolean succeeded = clearScrollResponse.isSucceeded();

    }

}
