package com.yore;

import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.ShardSearchFailure;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.avg.Avg;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.profile.ProfileResult;
import org.elasticsearch.search.profile.ProfileShardResult;
import org.elasticsearch.search.profile.aggregation.AggregationProfileShardResult;
import org.elasticsearch.search.profile.query.CollectorResult;
import org.elasticsearch.search.profile.query.QueryProfileShardResult;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.ScoreSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.search.suggest.Suggest;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.SuggestBuilders;
import org.elasticsearch.search.suggest.SuggestionBuilder;
import org.elasticsearch.search.suggest.term.TermSuggestion;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by yore on 2018-11-19 0:50
 */
public class SearchAPI {

    /**
     *
     * Search API
     * 搜索请求
     *
     * SearchRequest
     *     用于与搜索文档、聚合、建议有关的任何操作，
     *     并且还提供了在生成文档上的请求突出显示的方法。
     *
     * @date: 2018/11/19 0:53
     */
    @Test
    public void part6() throws IOException {
        RestHighLevelClient client = EsHighApi.getRestHighLevelClient();

        SearchRequest searchRequest = new SearchRequest();
        // 大多数的搜索参数被添加到SearchSourceBuilder。它为每个进入请求体的每个东西提供setter方法
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        // 添加一个match_all 查询到searchSourceBuilder
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());

        /**
         * 可选参数
         */
        // 限制请求的类型
        searchRequest.types("doc");

        // 设置路由参数
        searchRequest.routing("routing");

        // 设置lenientExpandOpen控制如何解析不可用的索引以及扩展通配符表达式
        searchRequest.indicesOptions(IndicesOptions.lenientExpandOpen());

        // 使用首选参数，例如，执行搜索优化选择本地分片，默认值随机化分片
        searchRequest.preference("_local");


        /**
         * 可以在SearchSourceBuilder上设置大多数控制搜索行为的选项，
         * 其中包含或多或少相当于Rest API的搜索请求正文中的选项
         */
        searchSourceBuilder.query(QueryBuilders.termQuery("user", "kimchy"));
        // 设置from选项，确定要开始搜索的结果索引，默认为0.
        searchSourceBuilder.from(0);
        // 设置大小选项，确定返回的搜索匹配数，默认为10
        searchSourceBuilder.size(5);
        // 设置一个可选的超时时间，用于控制搜索允许的时间
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));


        /**
         * 构建查询
         */
        // 创建一个字段user，与文本kimchy相匹配的全文索引查询
        MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("user", "kimchy");

        // 在匹配查询上启用模糊匹配
        matchQueryBuilder.fuzziness(Fuzziness.AUTO);
        // 在匹配插叙上设置前级长度
        matchQueryBuilder.prefixLength(3);
        // 设置最大扩展选项以控制查询的模糊过程
        matchQueryBuilder.maxExpansions(10);

        /* 也可使用QueryBuilders链式创建*/
        matchQueryBuilder = QueryBuilders.matchQuery("user", "kimchy")
                .fuzziness(Fuzziness.AUTO)
                .prefixLength(3)
                .maxExpansions(10);

        // 将QueryBuilder对象添加到SearchSourceBuilder
        searchSourceBuilder.query(matchQueryBuilder);


        /**
         * 指定排序
         */
        // 按_score降序排序，默认值
        searchSourceBuilder.sort(new ScoreSortBuilder().order(SortOrder.DESC));
        // 也按_id字段升序排序
        searchSourceBuilder.sort(new FieldSortBuilder("_uid").order(SortOrder.ASC));

        /*
        * 默认情况下，搜搜请求返回文档的内容，_source但像Rest API中的内容一样，
        * 你可以覆盖此行为。例如完全关闭_sorcerer检索
        */
        searchSourceBuilder.fetchSource(false);

        /*
        * 该方法还接收一个或多个统配符模式的数组，
        * 以便更精细的方式控制哪些字段包含或排除
        */
        String[] includeFields = new String[]{"title", "user", "innerObject.*"};
        String[] excludeFields = new String[]{"_type"};
        searchSourceBuilder.fetchSource(includeFields, excludeFields);

        /**
         * 请求高亮
         *
         * 突出显示搜索结果可以通过设置来实现HighlightBuilder的SearchSourceBuilder，
         * 可以通过向HighlightBuilder.Field添加一个或多个实例来为每一个字段定义不同的突出显示行为
         *
         */
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        // 创建一个title字段的高亮
        HighlightBuilder.Field hightlightTitle = new HighlightBuilder.Field("title");
        // 设置高亮显示的类型
        hightlightTitle.highlighterType("unified");
        highlightBuilder.field(hightlightTitle);

        HighlightBuilder.Field highlightUser = new HighlightBuilder.Field("user");
        highlightBuilder.field(highlightUser);

        searchSourceBuilder.highlighter(highlightBuilder);


        /**
         * 请求聚合
         *
         * 可以通过首先创建适当的聚合 ，然后在其上设置聚合来将搜索添加到搜索结果中。
         *
         * 下面的示例中，terms在公司名称上创建一个聚合，其中包含公司员工平均年龄的字聚合
         */
        TermsAggregationBuilder aggregationBuilder = AggregationBuilders.terms("by_company");
        aggregationBuilder.field("company.keyword");
        aggregationBuilder.subAggregation(AggregationBuilders.avg("average_age")).field("age");
        searchSourceBuilder.aggregation(aggregationBuilder);


        /**
         * 请求建议
         *
         */
        SuggestionBuilder termSuggestionBuilder = SuggestBuilders.termSuggestion("user").text("kmichy");
        SuggestBuilder suggestBuilder = new SuggestBuilder();
        suggestBuilder.addSuggestion("suggest_user", termSuggestionBuilder);
        searchSourceBuilder.suggest(suggestBuilder);


        /**
         * 自定义查询和聚合
         *
         */
        searchSourceBuilder.profile(true);



        /** 同步执行 */
        SearchResponse searchResponse = client.search(searchRequest);


        /** 异步执行 */
        client.searchAsync(searchRequest, new ActionListener<SearchResponse>() {
            @Override
            public void onResponse(SearchResponse searchResponse) {
                // 执行成功完成时调用
            }

            @Override
            public void onFailure(Exception e) {
                // 当整个SearchRequest失败时调用
            }
        });


        /* 搜索响应 */
        RestStatus status = searchResponse.status();
        TimeValue took = searchResponse.getTook();
        Boolean terminatedEarly = searchResponse.isTerminatedEarly();
        Boolean timeOut = searchResponse.isTimedOut();

        int totalShards = searchResponse.getTotalShards();
        int successfulShards = searchResponse.getSuccessfulShards();
        int failedShards = searchResponse.getFailedShards();
        for(ShardSearchFailure failure : searchResponse.getShardFailures()){
            // 故障应该在这里处理
        }


        /**
         * 检索 SearchHits
         * 要访问返回的文档，我们需要搜仙得到响应中包含的SearchHits
         */
        SearchHits hits = searchResponse.getHits();

        long totalHits = hits.getTotalHits();
        float maxScore = hits.getMaxScore();

        SearchHit[] searchHits = hits.getHits();
        for(SearchHit hit : searchHits){
            // 做一些与SearchHit的事情

            String index = hit.getIndex();
            String type = hit.getType();
            String id = hit.getId();
            float score = hit.getScore();

            /*
            * 此外，可以让文档源作为简单的JSON-String或键值对的映射。
            * 在该映射中，字段名为键，名字段值为值。
            * 多值字段作为对象的列表返回，嵌套对象作为另一个键值映射。
             */
            String sourceAsString = hit.getSourceAsString();
            Map<String, Object> sourceAsMap = hit.getSourceAsMap();
            String documentTitle = (String) sourceAsMap.get("title");
            List<Object> users = (List<Object>) sourceAsMap.get("user");
            Map<String, Object> innerObjecct = (Map<String, Object>) sourceAsMap.get("innerObject");

            // 高亮
            Map<String, HighlightField> highlightFields = hit.getHighlightFields();
            HighlightField highlight = highlightFields.get("title");
            Text[] fragments = highlight.getFragments();
            String fragmentsString = fragments[0].string();
        }


        /**
         * 检索聚合
         */
        Aggregations aggregations = searchResponse.getAggregations();
        Terms byCompanyAggregation = aggregations.get("by_company");
        Terms.Bucket elasticBucket = byCompanyAggregation.getBucketByKey("Elastic");
        Avg avergeAge = elasticBucket.getAggregations().get("average_age");
        double avg = avergeAge.getValue();

        /*
        * 如果按名称访问聚合，则需根据请求的聚合类型指定聚合接口，否则将抛出ClassCastException
        *
        * 这种方式将抛出异常，因为 "by_company"是一个term类型的聚合，但是我们试着将他作为range聚合
        * BinaryRangeAggregator.Range range = aggregations.get("by_company");
        */

        /* 也可以聚合名称键入的映射来访问所有的聚合 */
        Map<String, Aggregation> aggregationMap = aggregations.getAsMap();
        Terms companyAggregation = (Terms) aggregationMap.get("by_company");

        /* 还有getter方法将所有的顶级聚合作为列表返回 */
        List<Aggregation> aggregationList = aggregations.asList();

        /* 可以迭代所有的聚合，然后根据其类型决定如何进一步处理他们 */
        for(Aggregation agg : aggregations){
            String type = agg.getType();
            if(type.equals(TermsAggregationBuilder.NAME)){
                elasticBucket = ((Terms)agg).getBucketByKey("Elastic");
                long numberOfDocs = elasticBucket.getDocCount();

            }
        }


        /**
         * 检索建议
         */
        Suggest suggest = searchResponse.getSuggest();
        // Suggestion能够通过名字被关联上
        TermSuggestion termSuggestion = suggest.getSuggestion("suggest_user");
        // 迭代suggestion entries
        for(TermSuggestion.Entry entry : termSuggestion.getEntries()){
            // 迭代所有的在entries中的options
            for(TermSuggestion.Entry.Option option : entry){
                String suggestTest = option.getText().string();
            }
        }


        /**
         * 检索结果分析
         *
         * 从SearchResponse使用getProfileResults()方法检索分析结果。
         * 此方法返回Map包含执行中ProfileShardResult涉及的每一个分片的对象SearchRequest
         */
        // 遍历每一个分片的所有分析结果
        Map<String, ProfileShardResult> profilingResults = searchResponse.getProfileResults();
        for(Map.Entry<String, ProfileShardResult> profilingResult : profilingResults.entrySet()){
            // 接收一个属于分片的ProfileShardResult的key标识
            String key = profilingResult.getKey();
            // 接收ProfileShardResult的给定分片
            ProfileShardResult profileShardResult = profilingResult.getValue();

            List<QueryProfileShardResult> queryProfileShardResults = profileShardResult.getQueryProfileResults();
            // 迭代每一个QueryProfileShardResult
            for(QueryProfileShardResult queryProfileShardResult : queryProfileShardResults){
                // 迭代配置文件结果
                for(ProfileResult profileResult : queryProfileShardResult.getQueryResults()){
                    // 检索Lucene查询名称
                    String queryName = profileResult.getQueryName();
                    // 检索在执行Lucene查询话费的时间
                    long queryTimeInMillis = profileResult.getTime();
                    // 如果有，检索子查询配置文件结果
                    List<ProfileResult> profileChildren = profileResult.getProfiledChildren();
                }


                // 检索Lucene收集器的分析结果
                CollectorResult collectorResult = queryProfileShardResult.getCollectorResult();
                // 检索Lucene的名字
                String collectorName = collectorResult.getName();
                // 以毫秒计算的时间用于执行Lucene集合
                Long collectorTimeInMillis = collectorResult.getTime();
                List<CollectorResult> profiledChildren = collectorResult.getProfiledChildren();
            }


            /**
             * 检索自己和的个人资料结果（如果有），Rest API文档中包含Lucene收集器的分析信息的更多信息
             */
            AggregationProfileShardResult aggsProfileShardResult = profileShardResult.getAggregationProfileResults();
            for(ProfileResult profileResult : aggsProfileShardResult.getProfileResults()){
                String aggName = profileResult.getQueryName();
                long aggTimeInMillis = profileResult.getTime();
                // 如果有，检索子查询配置文件结果
                List<ProfileResult> profileChildren = profileResult.getProfiledChildren();
            }
        }

    }

}
