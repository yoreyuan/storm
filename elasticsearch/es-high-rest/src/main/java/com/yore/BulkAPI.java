package com.yore;

import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.bulk.*;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.support.ActiveShardCount;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.unit.ByteSizeUnit;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.threadpool.ThreadPool;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by yore on 2018/11/18 20:45
 */
@Slf4j
public class BulkAPI {

    /**
     *
     * Bulk API
     * Java高级REST客户端提供的批量处理器来协助大量请求
     *
     * BulkRequest可以被用在使用单个请求执行多个索引、更新和或者删除操作的请求
     * 它要求至少要一个操作添加到Bulk请求上
     *
     * @date: 2018/11/18 20:45
     */
    @Test
    public void part5() throws IOException, InterruptedException {
        RestHighLevelClient client = EsHighApi.getRestHighLevelClient();

        // ##Bulk API仅支持JSON或SMILE编码的文档。
        BulkRequest request = new BulkRequest();
        // 添加第一个IndexRequest到Bulk请求上
        request.add(new IndexRequest("posts", "doc", "1")
                .source(XContentType.JSON, "field", "foo")
        );
        // 添加第二个IndexRequest
        request.add(new IndexRequest("posts", "doc", "2")
                .source(XContentType.JSON, "field", "bar")
        );
        // 添加第三个IndexRequest
        request.add(new IndexRequest("posts", "doc", "3")
                .source(XContentType.JSON, "field", "baz")
        );


        /** 可以添加不同的操作类型 */
        request = new BulkRequest();
        // 添加一个删除的操作
        request.add(new DeleteRequest("posts", "doc", "3"));
        request.add(new UpdateRequest("posts", "doc", "2")
                .doc(XContentType.JSON, "other", "test")
        );
        // 添加一个IndexRequest,使用SMILE格式
        request.add(new IndexRequest("post", "doc", "4")
                .source(XContentType.JSON, "field", "baz")
        );


        /**
         * 可选参数
         */
        // 等待bulk请求并处理的超时时间
        request.timeout(TimeValue.timeValueMillis(2));
        //request.timeout("2m");

        // 刷新策略
        request.setRefreshPolicy(WriteRequest.RefreshPolicy.WAIT_UNTIL);
        //request.setRefreshPolicy("wait_for");

        // 设置数字，在执行 index、update、delete操作前分片复制必须激活，
        request.waitForActiveShards(2);
        request.waitForActiveShards(ActiveShardCount.ALL);


        /** 同步执行 */
        BulkResponse bulkResponse = client.bulk(request);

        /** 异步执行 */
        client.bulkAsync(request, new ActionListener<BulkResponse>() {
            @Override
            public void onResponse(BulkResponse bulkItemResponses) {
                // 当执行完成并成功的时候回调
            }

            @Override
            public void onFailure(Exception e) {
                // 当BulkRequest失败时回调
            }
        });


        /**
         * Bulk响应
         *
         * 返回的BulkResponse包含有关执行操作的信息，并允许对每一个结果进行迭代
         */
        // 迭代所有操作结果
        for(BulkItemResponse bulkItemResponse : bulkResponse){
            // 接收一个操作的请求（成功或不成功），
            // 可以是IndexResponse、UpdateResponse、DeleteResponse。所有继承DocWriteResponse
            DocWriteResponse itemResponse = bulkItemResponse.getResponse();
            if(bulkItemResponse.getOpType() == DocWriteRequest.OpType.INDEX ||
                    bulkItemResponse.getOpType() == DocWriteRequest.OpType.CREATE){
                // 处理一个Index操作的响应
                IndexResponse indexResponse = (IndexResponse) itemResponse;
            }else if(bulkItemResponse.getOpType() == DocWriteRequest.OpType.UPDATE){
                // 处理一个更新操作的响应
                UpdateResponse updateResponse = (UpdateResponse)itemResponse;
            }else if(bulkItemResponse.getOpType() == DocWriteRequest.OpType.DELETE){
                // 处理一个删除操作的响应
                DeleteResponse deleteResponse = (DeleteResponse)itemResponse;
            }
        }


        // 批量响应提供了一种更快速检查一个或多个操作是否失败的方法
        if(bulkResponse.hasFailures()){
            // 只要有一个操作失败了，这个方法就返回true

            //这种情况下需要迭代所有运算结果，已检查操作是否失败，如果是，则检索响应的故障
            for(BulkItemResponse bulkItemResponse : bulkResponse){
                if(bulkItemResponse.isFailed()){
                    // 表明一个给定的操作失败了

                    // 获取这个失败操作的失败对象
                    BulkItemResponse.Failure failure = bulkItemResponse.getFailure();
                }
            }
        }


        /**
         * Bulk处理器
         *
         * BulkProcessor通过提供允许Index、Update、Delete操作在添加到处理器时透明执行的使用程序类来简化Bulk API的使用
         * 为了执行请求，BulkProcessor需要3个组件：
         * ① RestHightLevelClient
         *      这个客户端用来执行BulkRequest并接收BulkResponse
         *
         * ② BulkProcessor.Listener
         *      这个监听器会在每个BulkRequest执行之前和之后被调用，或者当BulkRequest失败时调用
         *
         * ③ ThreadPool
         *      BulkRequest执行是使用这个池的线程完成的，允许BulkProcessor以非阻塞的方式工作，
         *      并允许在批量请求执行的同事接收新的Index、Update、Delete操作
         *
         *      然后BulkProcessor.Builder类可以被用来构建新的BulkProcessor
         *
         */
        Map<String, String> map = new HashMap<>();
        Settings settings = Settings.EMPTY;
        ThreadPool threadPool = new ThreadPool(settings);
        // 创建BulkProcessor.Listener
        BulkProcessor.Listener listener = new BulkProcessor.Listener() {
            @Override
            public void beforeBulk(long l, BulkRequest bulkRequest) {
                // 每个BulkRequest在执行之前调用的方法
            }

            @Override
            public void afterBulk(long l, BulkRequest bulkRequest, BulkResponse bulkResponse) {
                // 每个BulkRequest在执行之后调用的方法
            }

            @Override
            public void afterBulk(long l, BulkRequest bulkRequest, Throwable throwable) {
                // 当Bulkrequest失败回调的方法
            }
        };
        // 通过调用BulkProcessor.Builder的buil()方法创建BulkProcessor。
        // RestHighLevelClient.bulkAsync()将被用来执行BulkRequest
        BulkProcessor bulkProcessor = new BulkProcessor.Builder(
                client::bulkAsync,
                listener,
                threadPool).build();

        BulkProcessor.Builder builder = new BulkProcessor.Builder(client::bulkAsync, listener, threadPool);
        // 设置当刷新一个新的bulk请求，更具当前添加的执行的数字。默认是1000， use -1 同disable it
        builder.setBulkActions(500);

        // 设置当刷新一个新的bulk请求更具设置当前添加的大小。默认是5Mb，
        builder.setBulkSize(new ByteSizeValue(1L, ByteSizeUnit.MB));

        // 设置并发请求允许执行的数目。默认是1，use 0 to only allow单请求执行
        builder.setConcurrentRequests(0);

        builder.setFlushInterval(TimeValue.timeValueSeconds(10L));

        builder.setBackoffPolicy(BackoffPolicy.constantBackoff(TimeValue.timeValueSeconds(1L), 3));


        /* 可以向BulkProcessor,添加请求 */
        IndexRequest one = new IndexRequest("posts", "doc", "1")
                .source(XContentType.JSON, "title", "In which order are my Elasticsearch queries executed");
        IndexRequest two = new IndexRequest("posts", "doc", "2")
                .source(XContentType.JSON, "title", "Current status and upcoming changes in Elastisearch");
        IndexRequest three = new IndexRequest("posts", "doc", "3")
                .source(XContentType.JSON, "title", "The Future of Federated Search in Elasticsearch");
        bulkProcessor.add(one);
        bulkProcessor.add(two);
        bulkProcessor.add(three);


        /**
         * 这些请求将由BulkProcessor执行，它负责为每个批量请求用BulkProcessor.Listener
         *
         * 监听器提供方法接收BulkResponse和Bulkresponse
         */
        BulkRequest finalRequest = request;
        final org.slf4j.Logger logger = log;
        BulkProcessor.Listener listener1 = new BulkProcessor.Listener() {
            @Override
            public void beforeBulk(long executionId, BulkRequest bulkRequest) {
                int numberOfActions = finalRequest.numberOfActions();
                logger.debug("Executing bulk [{}] with {} requests", executionId, numberOfActions);
            }

            @Override
            public void afterBulk(long executionId, BulkRequest bulkRequest, BulkResponse bulkResponse) {
                if(bulkResponse.hasFailures()){
                    // 在每个BulkRequest执行之后调用，此方法允许获知BulkResponse是否包含错误
                    logger.warn("Bulk [{}] executed with failures", executionId);
                }else{
                    logger.debug("Bulk [{}] completed in {} mailliseconds", executionId, bulkResponse.getTook().getMillis());
                }
            }

            @Override
            public void afterBulk(long l, BulkRequest bulkRequest, Throwable failure) {
                logger.error("Failed to execute bulk : {}", failure);
            }
        };


        /**
         * 一旦将所有的请求都添加到BulkProcessor，其实例需要使用两种可用的关闭方法之一关闭。
         *
         * awaitClose()可以被用来等待到所有的请求都被处理，或者到指定的等待时间
         *      如果所有批量执行完成，返回true
         *      如果在完成所有批量请求之前等待时间过长，则返回false
         *
         * 在关闭之前两种方法都会刷新已经被添加到处理器的请求，并禁止添加任何新的请求
         */
        boolean terminated = bulkProcessor.awaitClose(30L, TimeUnit.SECONDS);

        //立即关闭
        bulkProcessor.close();

    }

}
