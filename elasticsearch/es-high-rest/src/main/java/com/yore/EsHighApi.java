package com.yore;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;

/**
 * Elasticsearch REST的高级API
 *
 * Created by yore on 2018/11/15 14:12
 */
public class EsHighApi {

    public static RestClientBuilder getRestClientBuilder(){
        return RestClient.builder(
                new HttpHost("i-ef32owrg", 9200, "http"),
                new HttpHost("hbm8wksw", 9200, "http"),
                new HttpHost("i-yxzzcusk", 9200, "http")
        );
    }

    /**
     * RestClient实例可以通过响应的RestClientBuilder类构建，
     * 通过静态方法RestClient.builder(HttpHost...)创建
     *
     * RestClient类是线程安全的，理想状态下它与使用它的应用程序具有相同的生命周期。
     * 当不再使用时强烈建议手动关闭：restClient.close();
     *
     * @auther: yore
     * @return RestClient
     * @date: 2018/10/27 9:46 AM
     */
    public static RestClient getRestClient(){
        return getRestClientBuilder().build();
    }

    /**
     * 获取Elasticsearch高级API（初始化REST实例）
     *
     * @auther: yore
     * @param
     * @return
     * @date: 2018/11/15 2:36 PM
     */
    public static RestHighLevelClient getRestHighLevelClient(){
        return new RestHighLevelClient(getRestClient());
    }


}
