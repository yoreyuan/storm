ElasticSearch Rest
---

## 二、Java高级REST客户端
Java高级REST客户端可以在Java Low Leverl REST客户机之上工作。其主要目标识公开特定方法的API，
接收请求对象作为参数并返回响应对象，以便客户端自己处理请求编组和响应解组。

每个API可以同步或异步地调用。
同步方法返回一个响应对象,
而名称以async后缀结尾的异步方法需要收到响应或错误后才会通知在低级别客户端管理的线程池上的侦听器参数。


### 2.1 起步
Java高级REST客户端依赖于Elasticsearch核心项目。它接收与TransportClient相同的请求参数，并返回相同的响应对象。

#### 兼容性
Java高级REST客户端需要**Java1.8**，并依赖于Elasticsearch核心项目。
客户端版本要与客户端开发的Elasticsearch版本相同。它会接受与TransportClient相同的请求参数，并返回相同的响应对象。

高级客户端保证能够运行在相同朱版本和大于或等于次要版本的任何Elasticsearch节点进行通信。

5.6客户端可以与任何5.6.x Elasticsearch节点进行通信。以前的5.x小版本，如5.5.x、5.4.x等不完全支持。

[Javadoc](https://artifacts.elastic.co/javadoc/org/elasticsearch/client/elasticsearch-rest-high-level-client/5.6.0/index.html)


#### Maven
```
<dependency>
    <groupId>org.elasticsearch.client</groupId>
    <artifactId>elasticsearch-rest-high-level-client</artifactId>
    <version>5.6.2</version>
    <type>pom</type>
</dependency>
```

### 2.2 支持的API
* 单一文档API
    * index API
    * Get API
    * Delete API
    * Updata API
* 多文档API
    * Bulk API
* 搜索API
    * Search API
    * Search Scroll API
    * Clear Scroll API
* 各种API
    * info API

#### Index API
Index

























