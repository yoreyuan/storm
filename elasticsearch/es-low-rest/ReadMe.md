ElasticSearch Rest
---
## 概览
Java REST客户端分为两种
  * java低级Rest
    通过Http协议与ElasticSearch服务进行通信。请求编码和响应解码保留给客户实现。
    与所有Elasticsearch版本兼容。
  
  * Java高级Rest
    Elasticsearch官方告诫客户端。基于低级客户端，
    提供特定的方法API，并处理请求编码和响应解码
    
## 一、Java低级REST客户端
功能：
   * 最小依赖
   * 负载均衡
   * 故障转移
   * 故障连接策略
   （是否重新连接故障节点取决于连续失败多少次；失败次数越多，再次尝试同一节点之前，客户端等待的时间越长）
   * 持久化连接
   * 跟踪记录请求和响应
   * 自动发现集群节点
    
### 1.1 起步
介绍了如何在程序中使用低级REST客户端
[ES Javadoc](https://artifacts.elastic.co/javadoc/org/elasticsearch/client/elasticsearch-rest-client/5.6.0/index.html) 


#### Maven
Java版本最低要求1.7
```
#Maven依赖
<dependency>
    <groupId>org.elasticsearch.client</groupId>
    <artifactId>rest</artifactId>
    <version>5.5.3</version>
</dependency>

#Gradle配置
dependencies{
    compile 'org.elasticsearch.client:elasticsearch-rest-client:5.4.3'
}

```


#### 日志记录
Java REST客户端使用和Apache Async Http客户端使用的相同日志记录库：Apache Commons Loggin，
它支持持许多流行的日志记录实现。用于启动日志记录的Java包是客户端本身的org.elasticsearch.client。

请求tracer日志记录可以开启一curl格式记录。
但是这种类型的日志非常消耗资源，不应一直在生产环境中启用，而只是在需要时暂时使用。

#### 其他
对于其他配置，请查阅[Apache HttpAsyncClient文档](https://hc.apache.org/httpcomponents-asyncclient-4.1.x/)


### 1.3 嗅探器
它是一个小型库，可以允许从一个正在运行的Elasticsearch集群上自动发现节点，并将节点列表更新到已经存在的RestClient实例上。
默认使用Nodes Info api检索属于集群的节点，并使用jackson解析获取的json响应，
与Elastics 2.x及以上版本兼容。

#### Javadoc
[REST客户端嗅探器的javadoc](https://artifacts.elastic.co/javadoc/org/elasticsearch/client/elasticsearch-rest-client-sniffer/5.6.0/index.html)

#### Maven
REST客户端嗅探器与Elasticsearch的发行版本周期相同。
```
<!-- https://mvnrepository.com/artifact/org.elasticsearch.client/elasticsearch-rest-client-sniffer -->
<dependency>
    <groupId>org.elasticsearch.client</groupId>
    <artifactId>elasticsearch-rest-client-sniffer</artifactId>
    <version>5.6.2</version>
</dependency>
```

#### 用法
一旦创建了RestClient实例，就可以将嗅探器与之相关联。
Sniffer将使用关联的RestClient定期（默认每5分钟）从集群中获取当前可用的节点列表，并通过用RestClient.setHosts来更新

```
Sniffer sniffer = Sniffer.builder(restClient)
     // 设置更新节点的时间间隔
     .setSniffIntervalMillis(60000)
     .build();
        
sniffer.close();
restClient.close();
```
