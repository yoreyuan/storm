package com.yore;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.RequestLine;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.http.impl.nio.reactor.IOReactorConfig;
import org.apache.http.message.BasicHeader;
import org.apache.http.nio.entity.NStringEntity;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.client.*;
import org.elasticsearch.client.sniff.ElasticsearchHostsSniffer;
import org.elasticsearch.client.sniff.HostsSniffer;
import org.elasticsearch.client.sniff.SniffOnFailureListener;
import org.elasticsearch.client.sniff.Sniffer;
import org.junit.Test;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by yore on 2018/10/26 17:12
 */
public class EsLowApiDemo {

    /**
     * RestClient实例可以通过响应的RestClientBuilder类构建，
     * 通过静态方法RestClient.builder(HttpHost...)创建
     *
     * RestClient类是线程安全的，理想状态下它与使用它的应用程序具有相同的生命周期。
     * 当不再使用时强烈建议手动关闭：restClient.close();
     *
     * @auther: yore
     * @return RestClient
     * @date: 2018/10/27 9:46 AM
     */
    public static RestClient getRestClient(){
        return getRestClientBuilder().build();
    }

    public static RestClientBuilder getRestClientBuilder(){
        return RestClient.builder(
                new HttpHost("i-ef32owrg", 9200, "http"),
                new HttpHost("hbm8wksw", 9200, "http"),
                new HttpHost("i-yxzzcusk", 9200, "http")
        );
    }

    @Test
    public void part1() throws IOException {

        RestClient restClient = getRestClient();

        RestClientBuilder builder = getRestClientBuilder();
        Header[] defaultHeaders = new Header[]{
                new BasicHeader("header", "value")
        };

        /*
        * 设置默认投文件，避免每次请求都必须指定
        */
        builder.setDefaultHeaders(defaultHeaders);

        /*
        * 设置在同一请求进行多次尝试时应该遵守的超时时间。
        * 默认是30秒，与默认的socker超时时间相同。
        * 如果自定义设置了socket超时，则应该响应地调整最大重试超时
        *
        */
        builder.setMaxRetryTimeoutMillis(10000);

        builder.setFailureListener(new RestClient.FailureListener(){
            @Override
            public void onFailure(HttpHost host) {
                super.onFailure(host);
                /*
                * 设置每次节点发生故障时收到同时的侦听器。
                * 内部嗅探到故障时被启动。
                */
            }
        });

        builder.setRequestConfigCallback(new RestClientBuilder.RequestConfigCallback() {
            @Override
            public RequestConfig.Builder customizeRequestConfig(RequestConfig.Builder requestConfigBuilder) {
                /**
                * 可以修改默认请求配置的回调。例如：请求超时、认证、或者其他
                *
                * @see <a href="https://hc.apache.org/httpcomponents-client-ga/httpclient/apidocs/org/apache/http/client/config/RequestConfig.Builder.html">Class RequestConfig.Builder</a>
                */
//                return null;
                return requestConfigBuilder.setSocketTimeout(10000);
            }
        });

        builder.setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
            @Override
            public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpClientBuilder) {
                /**
                 * 设置修改http客户端配置的回调。例如：ssl加密通讯，或其他任何
                 *
                 * @see <a href="https://hc.apache.org/httpcomponents-asyncclient-dev/httpasyncclient/apidocs/org/apache/http/impl/nio/client/HttpAsyncClientBuilder.html">Class HttpAsyncClientBuilder</a>
                 */
                return httpClientBuilder.setProxy(new HttpHost("proxy", 9000, "http"));
            }
        });


        //手动关闭
        restClient.close();
    }


    @Test
    public void part2() throws IOException {
        /**
         * 一旦创建了RestClient，就可以通过调用其中一个performRequest或performRequestAsync方法来发送请求
         *
         * performRequest是一个同步的，直接返回Response，客户端将阻塞并等待返回响应
         * performRequestAsync返回void，可以接受一个额外的ResponseListener作为参数，是一个异步执行的，传入的监听器将在请求完成或失败时通知。
         *
         */
        RestClient restClient = getRestClient();
        // 最简单的发送一个请求
        Response response = restClient.performRequest("GET", "/");

        // 发送一个带参数的请求
        Map<String, String> params = Collections.singletonMap("pretty", "true");
        Response response1 = restClient.performRequest("GET", "/", params);

        Map<String, String> params2 = Collections.emptyMap();
        String jsonString = "{" +
                "\"user\": \"kimchy\", " +
                "\"postDate\": \"2018-11-09\", " +
                "\"message\": \"trying out Elasticsearch\" " +
              "}";
        // org.apache.http.HttpEntity 为了让Elasticsearch能够解析，需要设置ContentType
        HttpEntity entity = new NStringEntity(jsonString, ContentType.APPLICATION_JSON);
        Response response2 = restClient.performRequest("PUT", "/posts/doc/1", params2, entity);
        System.out.println(response2);


        // 创建一个不可变的Map空对象
        Map<String, String> params3 = Collections.emptyMap();
        /**
         * 设置修改http客户端配置的回调。例如：ssl加密通讯，或其他任何
         *
         * @see <a href="https://hc.apache.org/httpcomponents-core-ga/httpcore-nio/apidocs/org/apache/http/nio/protocol/HttpAsyncResponseConsumer.html">Interface HttpAsyncResponseConsumer&lt;T&gt; </a>
         */
        HttpAsyncResponseConsumerFactory.HeapBufferedResponseConsumerFactory consumerFactory =
                new HttpAsyncResponseConsumerFactory.HeapBufferedResponseConsumerFactory(30*1024*1024);
        Response response3 = restClient.performRequest("GET", "/posts/_search", params3, null, consumerFactory);


        ResponseListener responseListener = new ResponseListener() {
            @Override
            public void onSuccess(Response response) {
                // 请求成功回调
            }

            @Override
            public void onFailure(Exception exception) {
                // 请求失败回调
            }
        };
        // 发送一个异步请求
        restClient.performRequestAsync("GET", "/", responseListener);

        // 发送一个带参数的异步请求
        Map<String, String> params4 = Collections.singletonMap("pretty", "true");
        restClient.performRequestAsync("GET", "/", params4, responseListener);


        HttpEntity entity1 = new NStringEntity(jsonString, ContentType.APPLICATION_JSON);
        restClient.performRequestAsync("PUT", "/posts/doc/1", params, entity1, responseListener);


        restClient.performRequestAsync("GET", "/posts/_search", params3, null, consumerFactory, responseListener);


        Header[] headers = {
                new BasicHeader("header1", "values1"),
                new BasicHeader("header2", "values2")
        };
        restClient.performRequestAsync("GET", "/", responseListener, headers);
    }


    /**
     * 发送异步请求
     *
     * @auther: yore
     * @date: 2018/11/14 6:47 PM
     */
    @Test
    public void part3() throws InterruptedException {
        RestClient restClient = getRestClient();

        HttpEntity[] document = new HttpEntity[]{};

        final CountDownLatch latch = new CountDownLatch(document.length);

        for(int i=0; i<document.length; i++){
            restClient.performRequestAsync(
                    "PUT",
                    "/posts/doc/" + i,
                    Collections.<String, String>emptyMap(),
                    document[i],
                    new ResponseListener() {
                        @Override
                        public void onSuccess(Response response) {
                            // 处理返回响应
                            latch.countDown();
                        }

                        @Override
                        public void onFailure(Exception exception) {
                            // 处理失败响应，exception里带错误代码
                            latch.countDown();
                        }
                    }
            );

        }

        latch.await();

    }


    /**
     * 读取响应
     *
     * <pre>
     *    返回Response对象
     *      performRequest,方法返回
     *      ResponseListener.onSucess(Response)接收
     *
     *    请求时可能会抛出一下异常（或者ResponseListener.onFailure(Exception)参数接收错误信息）
     *      IOException
     *        通信问题（例如：SocketTimeoutException）
     *
     *      ResponseException
     *        返回了一个response，但是它的状态码显示了一个错误（不是2xx）。说明连接是通的。
     *        对于返回404状态码的请求，不会抛出ResponseException，因为这是一个预期的响应，只是表示找不到该资源。除非ignore参数包含404.
     * </pre>
     * @auther: yore
     * @date: 2018/11/14 6:47 PM
     */
    @Test
    public void part4() throws IOException {
        RestClient restClient = getRestClient();

        Response response = restClient.performRequest("GET", "/");
        // 请求信息
        RequestLine requestLine = response.getRequestLine();
        // 返回response host信息
        HttpHost host = response.getHost();
        // 返回状态行，获取状态吗
        int statusCode = response.getStatusLine().getStatusCode();
        // response headers，也可以通过名字获取`getHeader(String)`
        Header[] headers = response.getHeaders();
        //response org.apache.http.HttpEntity对象
        String responseBody = EntityUtils.toString(response.getEntity());

    }


    /**
     * 超时
     *
     * @auther: yore
     * @date: 2018/11/14 7:49 PM
     */
    @Test
    public void part5() throws IOException {
        RestClientBuilder builder = getRestClientBuilder();
        builder.setRequestConfigCallback(new RestClientBuilder.RequestConfigCallback() {
            @Override
            public RequestConfig.Builder customizeRequestConfig(RequestConfig.Builder requestConfigBuilder) {
                return requestConfigBuilder.setConnectTimeout(5000).setSocketTimeout(60000);
            }
        });
        builder.setMaxRetryTimeoutMillis(60000);
    }


    /**
     * 线程数
     * Apache Http Async Client默认启动一个调度线程，连接管理器使用多个worker线程，
     * 线程的数量和CPU核数量相同（灯都Runtime.getRuntime().availableProcessors()返回的数量）
     *
     * @auther: yore
     * @date: 2018/11/14 7:49 PM
     */
    @Test
    public void part6() {
        RestClientBuilder builder = getRestClientBuilder();
        builder.setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback(){
            @Override
            public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpClientBuilder) {
                return httpClientBuilder.setDefaultIOReactorConfig(
                        IOReactorConfig.custom().setIoThreadCount(1).build()
                );
            }
        });
    }


    /**
     * 基本认证
     *
     * @auther: yore
     * @date: 2018/11/14 8:02 PM
     */
    @Test
    public void part7() {
        final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials("user", "password"));

        RestClientBuilder builder = getRestClientBuilder();
        builder.setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
            @Override
            public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpClientBuilder) {
                // 可以通过HttpAsyncClientBuilder来禁用。
//                httpClientBuilder.disableAuthCaching();

                return httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
            }
        });
    }

    /**
     * 加密传输
     *
     * 如果没有提供明确的配置，则使用系统默认配置。
     *
     * @auther: yore
     * @date: 2018/11/14 8:02 PM
     */
    @Test
    public void part8() throws KeyStoreException, IOException, CertificateException,
            NoSuchAlgorithmException, KeyManagementException {
        File keyStorePath = new File("");
        String keyStorePass = "";
        KeyStore truststore = KeyStore.getInstance("jks");

        try(InputStream is = Files.newInputStream(keyStorePath.toPath())){
            truststore.load(is, keyStorePass.toCharArray());
        }

        SSLContextBuilder sslBuilder = SSLContexts.custom().loadTrustMaterial(truststore, null);
        final SSLContext sslContext = sslBuilder.build();
        RestClientBuilder builder = getRestClientBuilder();
        builder.setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
            @Override
            public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpClientBuilder) {
                return httpClientBuilder.setSSLContext(sslContext);
            }
        });

    }


    /**
     * 嗅探器
     *
     * @auther: yore
     * @date: 2018/11/15 12:55 AM
     */
    @Test
    public void part9() throws IOException {

        /*RestClient restClient = getRestClient();
        Sniffer sniffer = Sniffer.builder(restClient)
            // 设置更新节点的时间间隔
            .setSniffIntervalMillis(60000)
            .build();*/

        /**
         * 也可以在发生故障时启用嗅探器，
         * 这时SniffOnFailureListener需要首先被创建，并将实例在RestClient创建时提供给它。
         *
         * 故障后嗅探，默认情况是故障之后1分钟后，假设节点被恢复正常，那么我们希望尽可能快的获知。
         *
         */
        SniffOnFailureListener sniffOnFailureListener = new SniffOnFailureListener();
        RestClient restClient = getRestClientBuilder()
                .setFailureListener(sniffOnFailureListener).build();
        Sniffer sniffer = Sniffer.builder(restClient)
                .setSniffAfterFailureDelayMillis(30000).build();
        //将嗅探器关联到嗅探故障监听器上。
        sniffOnFailureListener.setSniffer(sniffer);


        /**
         * Elasticsearch Nodes Info api 不会返回连接节点使用的协议，而只有host：port键值时，
         * 因此默认使用http。
         *
         * 如果需要使用https，必须手动创建和提供ElastisearchHostsSniffer实例
         */
        HostsSniffer hostsSniffer = new ElasticsearchHostsSniffer(
                restClient,
                ElasticsearchHostsSniffer.DEFAULT_SNIFF_REQUEST_TIMEOUT,
                ElasticsearchHostsSniffer.Scheme.HTTPS
        );
        Sniffer sniffer1 = Sniffer.builder(restClient)
                .setHostsSniffer(hostsSniffer).build();


        /**
         * 同样的方式，可以自定义设置sniffRequestTimeout参数，该参数默认值为1秒。
         *
         * 这样当服务端超时时，仍然会返回一个有效的响应，虽然它可能仅包含属于集群的一部分节点，其他节点会在随后响应。
         */
        HostsSniffer hostsSniffer2 = new ElasticsearchHostsSniffer(
                restClient,
                TimeUnit.SECONDS.toMillis(5),
                ElasticsearchHostsSniffer.Scheme.HTTP
        );
        Sniffer sniffer2 = Sniffer.builder(restClient)
                .setHostsSniffer(hostsSniffer2).build();


        /**
         * 同样的，一个自定义的HostsSniffer实现可以提供一个高级的用法功能，
         * 比如可以从Elasticsearch之外的来源获取主机
         */
        HostsSniffer hostsSniffer3 = new HostsSniffer() {

            @Override
            public List<HttpHost> sniffHosts() throws IOException {
                // 从外部源获取主机
                return null;
            }
        };
        Sniffer sniffer3 = Sniffer.builder(restClient)
                .setHostsSniffer(hostsSniffer3).build();


        sniffer.close();
        restClient.close();
    }


}
