package com.yoreyuan;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.hdfs.bolt.HdfsBolt;
import org.apache.storm.hdfs.bolt.format.DefaultFileNameFormat;
import org.apache.storm.hdfs.bolt.format.DelimitedRecordFormat;
import org.apache.storm.hdfs.bolt.format.FileNameFormat;
import org.apache.storm.hdfs.bolt.format.RecordFormat;
import org.apache.storm.hdfs.bolt.rotation.FileRotationPolicy;
import org.apache.storm.hdfs.bolt.rotation.FileSizeRotationPolicy;
import org.apache.storm.hdfs.bolt.sync.CountSyncPolicy;
import org.apache.storm.hdfs.bolt.sync.SyncPolicy;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.apache.storm.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Random;

/**
 * 官方文档参考：@see <a href="http://storm.apache.org/releases/1.2.2/storm-hdfs.html">Storm HDFS Integration</a>
 *
 * Created by yore on 2018/10/22 17:46
 */
public class HdfsSpoutTopology {

    public static void main(String[] args) {
        // use "|" instead of "," for field delimiter
        RecordFormat format = new DelimitedRecordFormat()
                .withFieldDelimiter("|");

        // sync the filesystem after every 1k tuples
        SyncPolicy syncPolicy = new CountSyncPolicy(100);

        // rotate files when they reach 5MB
        // 当文件到达5MB时触发写操作。
        FileRotationPolicy rotationPolicy = new FileSizeRotationPolicy(5.0f, FileSizeRotationPolicy.Units.MB);

        FileNameFormat fileNameFormat = new DefaultFileNameFormat()
                .withPath("/test/storm/");

        HdfsBolt hdfsBolt = new HdfsBolt()
                .withFsUrl("hdfs://cdh6.ygbx.com:8020")
                .withFileNameFormat(fileNameFormat)
                .withRecordFormat(format)
                .withRotationPolicy(rotationPolicy)
                .withSyncPolicy(syncPolicy);


        TopologyBuilder builder = new TopologyBuilder();
        builder.setSpout("data_sources_spout", new DataSourcesSpout(), 1);
        builder.setBolt("split_bolt", new SplitBolt(), 1)
                .localOrShuffleGrouping("data_sources_spout");
        builder.setBolt("hdfs_bolt", hdfsBolt)
                .localOrShuffleGrouping("split_bolt");

        Config config = new Config();
        config.setNumWorkers(3);
        config.setMessageTimeoutSecs(10);
        config.setMaxSpoutPending(128);
        config.setDebug(false);

        // is run Strom Tomplogy in Local
        boolean isLocal = true;
        if(isLocal){
            // local
            LocalCluster cluster = new LocalCluster();
            // Submit Topology
            cluster.submitTopology("HDFS-topology", config, builder.createTopology());
        }else {
            try {
                StormSubmitter.submitTopology("HDFS-topology", config, builder.createTopology());
            } catch (AlreadyAliveException | InvalidTopologyException | AuthorizationException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 数据源，
     *
     * Created by yore on 2018/10/25 15:46
     */
    public static class DataSourcesSpout extends BaseRichSpout{
        private static final Logger LOG = LoggerFactory.getLogger(DataSourcesSpout.class);

        private SpoutOutputCollector collector;

        public static final String[] sentencesArr = new String[]{
                "the cow jumped over the moon",
                "the man went to the store and bought some candy",
                "four score and seven years ago",
                "how many apples can you eat",
                "to be or not to be the person"
        };

        @Override
        public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
            this.collector = collector;
        }

        @Override
        public void nextTuple() {
            Random random = new Random();
            String sentence = sentencesArr[random.nextInt(sentencesArr.length)];

            collector.emit(new Values(sentence));

            LOG.info("## emit sentence:{}", sentence);

            Utils.sleep(500);
        }

        @Override
        public void ack(Object msgId) {
            super.ack(msgId);
            LOG.info("$$ ack \tmsgId={}", msgId);
        }

        @Override
        public void fail(Object msgId) {
            super.fail(msgId);
            LOG.info("$$ fail \tmsgId={}", msgId);
        }

        @Override
        public void declareOutputFields(OutputFieldsDeclarer declarer) {
            declarer.declare(new Fields("line"));
        }

    }

    /**
     * 切分数据，
     *
     * Created by yore on 2018/10/25 17:14
     */
    public static class SplitBolt extends BaseRichBolt{
        private OutputCollector collector;

        @Override
        public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
            this.collector = collector;
        }

        @Override
        public void execute(Tuple input) {
            for(String word : input.getStringByField("line").split("\\s")){
                collector.emit(input, new Values(word));
            }
            collector.ack(input);
        }

        @Override
        public void declareOutputFields(OutputFieldsDeclarer declarer) {
            declarer.declare(new Fields("word"));
        }
    }


}
