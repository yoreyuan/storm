Kudu文档
--------

## 一、入门：
查看集群是否已经成功安装kudu和impala
```
ps aux | grep kudu
ps aux | grep impalad
```

#### 1.1 加载数据
[旧金山MTA GPS数据集](ftp://avl-data.sfmta.com/avl_data/avl_raw)

```
#原始数据集使用DOS样式的行结尾，因此我们将在上载过程中使用它将其转换为UNIX样式tr。
$ wget http://kudu-sample-data.s3.amazonaws.com/sfmtaAVLRawData01012013.csv.gz
$ hdfs dfs -mkdir /sfmta
$ zcat sfmtaAVLRawData01012013.csv.gz | tr -d '\r' | hadoop fs -put - /sfmta/data.csv
```

#### 1.2 建表
创建新的外部Impala表以访问纯文本数据
tbl Properties 表格设定属性

建表语句
```
CREATE [EXTERNAL] TABLE [IF NOT EXISTS] [db_name.]table_name
  (col_name data_type [COMMENT 'col_comment'], ...)
  [PARTITIONED BY (col_name data_type [COMMENT 'col_comment'], ...)]
  [COMMENT 'table_comment']
  [WITH SERDEPROPERTIES ('key1'='value1', 'key2'='value2', ...)]
  [
   [ROW FORMAT row_format] [STORED AS file_format]
  ]
  [LOCATION 'hdfs_path']
  [TBLPROPERTIES ('key1'='value1', 'key2'='value2', ...)]
  [CACHED IN 'pool_name' [WITH REPLICATION = integer] | UNCACHED]
```
* 内部表
   默认 
   转外部表
   `aalter table 内部表 set tblproperties('external'='false'')`
* 外部表
   external
   转内部表     `aalter table 内部表 set tblproperties('external'='true'')`
   查看外部表属性 `describe extended tablename;`

```
CREATE external TABLE sfmta_raw (
  revision int,
  report_time string,
  vehicle_tag int,
  longitude float,
  latitude float,
  speed float,
  heading float
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','
LOCATION '/tmp/sfmta/'
TBLPROPERTIES ('skip.header.line.count'='1');
```

创建一个Kudu表并加载数据，
我们的report_time字段是unix样式的时间戳，
```
#目前不支持在外部表中加载数据
CREATE TABLE sfmta
PRIMARY KEY (report_time, vehicle_tag)
PARTITION BY HASH(report_time) PARTITIONS 8
STORED AS KUDU
AS SELECT
  UNIX_TIMESTAMP(report_time,  'MM/dd/yyyy HH:mm:ss') AS report_time,
  vehicle_tag,
  longitude,
  latitude,
  speed,
  heading
FROM sfmta_raw;
```

###### impala对DELETE语句的，
需要impala为 2.8或更高版本，并且需要和KUDU集成后才能使用

