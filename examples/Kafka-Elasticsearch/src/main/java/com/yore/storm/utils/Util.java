package com.yore.storm.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONPath;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by yore on 2018/11/19 15:47
 */
@Slf4j
public class Util {

    /**
     * 解析JSON，返回解析后的对象，
     * 如果传入参数为空，返回null; 如果传入数据不为json格式，返回为null;
     * @author: yore
     * @param jsonStr 字符串
     * @return com.alibaba.fastjson.JSONObject
     *      字符串转成后JSON对象，为空或者不是JSON格式数据返回null
     * @date: 2018/7/25 12:40
     */
    public static JSONObject getParseJson(String jsonStr){
        if(StringUtils.isBlank(jsonStr)){
            return null;
        }
        try {
            return JSON.parseObject(jsonStr);
        }catch (Exception e){
            log.error("解析JSON发生异常");
            return null;
        }
    }


    /**
     * 去除空字符
     * <pre>
     *   1.摘要结果：
     *     去除裁判文书中的空字符（包括无实际意义的字符）
     *
     *   2.案号：
     *    去除案号或者裁判文书原文中的空字符，
     *    在文书案号和内容匹配时尽可能减除空字符的干扰
     *
     * </pre>
     *
     * @author: yore
     * @param caseNoOrJudgeContent 案号或者裁判内容
     * @return java.lang.String 去除空字符的案号或者裁判内容(括号已格式化为中文括号)
     * @date: 2018/8/21 9:34
     */
    public static String removeBlankCaseNoAndJudegContent(String caseNoOrJudgeContent){
        return StringUtils.isBlank(caseNoOrJudgeContent)? "" :
                caseNoOrJudgeContent.replace(" ","")
                        .replace("　","")
                        .replace("<p>","")
                        .replace("amp;","")
                        .replace("(","（")
                        .replace(")","）")
                        .replaceAll("\\s*|\t|\r|\n","");
    }


    /**
     * 获取JSON数据中的裁判文书json对象
     * <pre>
     *     其中参数type:
     *       1-persion
     *       2-company
     *       3-companyName&type
     *       其他值-persion&company
     * </pre>
     *
     * @param jsonObject -json对象,
     * @param type 返回数据类型 <b> 1-persion  2-company 3-company&type 4-companyName&type 其他值-persion&company</b>
     * @return List<JSONObject> 裁判文书JSON对象。
     *      如果节点为搜索到，会返回空集合[], 不会返回null对象。
     * @author: yore
     * @date: 2018/7/30 18:50
     */
    public static List<JSONObject> getCpwsJsonObjList(JSONObject jsonObject, Integer type){
        List<JSONObject> cpwsJsonList = new ArrayList<>();

        if(1==type){
            // 个人
            cpwsJsonList = (List<JSONObject>) JSONPath
                    .eval(jsonObject,"$.resultJson.person.personArray.cpws");
        }else if(2==type){
            // 企业
            cpwsJsonList = (List<JSONObject>)JSONPath
                    .eval(jsonObject,"$.resultJson.company.companyArray.cpws");
        }else if(3 == type){
            // 带企业类型的裁判文书
            JSONArray cJSONArray ;
            try {
                cJSONArray = jsonObject.getJSONObject("resultJson")
                        .getJSONObject("company")
                        .getJSONArray("companyArray");
                for(Object companyOjb: cJSONArray){
                    JSONObject companyJsonOjb = (JSONObject)companyOjb;
                    // 获取公司类型
                    String comType = companyJsonOjb.getString("type")==null? "":
                            companyJsonOjb.getString("type");
                    // 公司姓名
                    String comName = companyJsonOjb.getString("companyName")==null? "":
                            companyJsonOjb.getString("companyName");
                    // 裁判文书对象
                    JSONArray cpwsJsonArr = companyJsonOjb.getJSONArray("cpws");

                    if(cpwsJsonArr!=null || cpwsJsonArr.size()>0){
                        // 有裁判文书内容
                        for(Object cpwsJsonO: cpwsJsonArr){
                            JSONObject cpwsJsonO2 = (JSONObject) cpwsJsonO;
                            cpwsJsonO2.put("type", comType);
                            cpwsJsonO2.put("companyName", comName);
                            cpwsJsonList.add(cpwsJsonO2);
                        }
                    }
                }
            }catch (Exception e){
                log.error("json在解析企业cpws是发生了异常：{}",e.getMessage());
            }

        }else{
            /* 个人和企业一起返回 */
            // 获取个人JSON集合，并放入到裁判文书JSON集合中
            cpwsJsonList = (List<JSONObject>) JSONPath
                    .eval(jsonObject,"$.resultJson.person.personArray.cpws");
            // 获取企业裁判文书JSON集合
            List<JSONObject> companyCpwsJsonObjList = (List<JSONObject>)JSONPath
                    .eval(jsonObject,"$.resultJson.company.companyArray.cpws");
            //将企业JSON集合放入到裁判文书JSON集合中
            cpwsJsonList.addAll(companyCpwsJsonObjList);
        }

        // 根据案号对裁判文书进行去重
        return /*cpwsJsonList*/ distinctCpwsByCaseNo(cpwsJsonList, type);
    }

    /**
     * 根据案号去重裁判文书，
     * 如果同一案号多个裁判文书，取去除空字符后最长的文书
     *
     * @author: yore
     * @param cpwsJsonList 裁判文书对象的集合
     * @param  type 接口类型 1-个人  2-企业     3-企业
     * @return java.util.List<com.alibaba.fastjson.JSONObject> 去重后的裁判文书结合
     * @date: 2018/8/22 15:57
     */
    private static List<JSONObject> distinctCpwsByCaseNo(List<JSONObject> cpwsJsonList, Integer type){
        if(null == cpwsJsonList)return null;
        if(type == 2)return  cpwsJsonList;

        // key=案号   value=文书对象list
        ConcurrentHashMap<String,List<JSONObject>> cpwsMap = new ConcurrentHashMap<>();

        if(1 == type){    // 个人
            for(JSONObject persionCpwsJsonObj : cpwsJsonList){
                // 案号去空字符
                String caseNo = Util.removeBlankCaseNoAndJudegContent(persionCpwsJsonObj.getString("caseNo"));
                caseNo = "11&"+caseNo+ "&" + persionCpwsJsonObj.getString("caseType");
                List<JSONObject> list = cpwsMap.get(caseNo);
                list = list==null? new ArrayList<JSONObject>():list;
                list.add(persionCpwsJsonObj);
                cpwsMap.put(caseNo,list);
            }
        }else if(3 == type ){   // 企业
            for(JSONObject companyCpwsJsonObj : cpwsJsonList){
                String caseNo = Util.removeBlankCaseNoAndJudegContent(companyCpwsJsonObj.getString("caseNo"));
                // 一人在同家公司多种角色时的同一文书视为不同文书，所以 key=角色+案号
                caseNo = getCompanyType(companyCpwsJsonObj.getString("type"))+"&"+caseNo + "&" + companyCpwsJsonObj.getString("caseType");
                List<JSONObject> list = cpwsMap.get(caseNo);
                list = list==null? new ArrayList<JSONObject>():list;
                list.add(companyCpwsJsonObj);
                cpwsMap.put(caseNo,list);
            }
        }


        /**
         * 循环执行两次
         * 针对entry.key（同一案件类型、同一案号）对应的List<JSONObject>执行两次，
         * 第一次：对compayName相同的文书去重，留下文书内容较多的那条文书
         * 第二次：对compayName不同的文书进行合并，合并时同样以文书内容最多的为准
         */
        cpwsJsonList = new ArrayList<>();
        for(int i=0; i<2; i++){
            for(Map.Entry<String,List<JSONObject>> entry:cpwsMap.entrySet()){
                if(null==entry.getValue())continue;
                int cpwsCount = entry.getValue().size();

                if(cpwsCount==1){
                    if(i==1) {
                        JSONObject o2 = entry.getValue().get(0);
                        if(type == 3 ) o2.put("companyName", Util.removeBlankCaseNoAndJudegContent(o2.getString("companyName")));
                        cpwsJsonList.add(o2);
                    }
                }else if(cpwsCount>1){
                    if(i==0){
                        // 第一次，此时key=entry.key&companyName
                        ConcurrentHashMap<String,List<JSONObject>> companyNameMap = new ConcurrentHashMap<>();
                        for(JSONObject o1 : entry.getValue()){
                            String keyWithCompany = entry.getKey() + "&" + o1.getString("companyName");
                            List<JSONObject> sameComanyCpwsList = companyNameMap.get(keyWithCompany);
                            if (sameComanyCpwsList == null)sameComanyCpwsList = new ArrayList<>();
                            sameComanyCpwsList.add(o1);
                            companyNameMap.put(keyWithCompany, sameComanyCpwsList);
                        }
                        List<JSONObject> cpwsJsonList2 = new ArrayList<>();
                        for(Map.Entry<String,List<JSONObject>> entry1 : companyNameMap.entrySet()){
                            if(entry1.getValue().size()==1){
                                cpwsJsonList2.add(entry1.getValue().get(0));
                            }else{
                                int maxCpwsIndex = getMaxBodyLenIndex(entry1.getValue());
                                cpwsJsonList2.add(entry1.getValue().get(maxCpwsIndex));
                            }
                        }

                        cpwsMap.replace(entry.getKey(), cpwsJsonList2);
                    }else if(i==1){
                        // compayName不同的文书进行合并
                        int maxCpwsIndex = getMaxBodyLenIndex(entry.getValue());
                        JSONObject o2 = entry.getValue().get(maxCpwsIndex);
                        String companyName2 = o2.getString("companyName");
                        for(int j=0; j<entry.getValue().size(); j++){
                            if(j!=maxCpwsIndex) companyName2 += "&," + entry.getValue().get(j).getString("companyName");
                        }
                        o2.put("companyName", Util.removeBlankCaseNoAndJudegContent(companyName2));
                        cpwsJsonList.add(o2);
                    }
                }
            }
        }
        return cpwsJsonList;
    }

    /**
     * 获取企业的案件性质
     * <pre>
     *    [担任法人, 担任投资人, 任职]
     *    对应的是
     *    [任法人企业,任投资企业,任职高管]
     * </pre>
     *
     * @author: yore
     * @param companyType  企业类型
     * @return String 企业的案件性质
     *      (21-任法人企业案件、22-任高管企业案件、23-投资企业案件)
     */
    public static String getCompanyType(String companyType) {
        switch (companyType) {
            // 任法人企业案件
            case "担任法人" : companyType = "21"; break;
            // 任高管企业案件
            case "任职" : companyType = "22"; break;
            // 投资企业案件
            case "担任投资人" : companyType = "23"; break;
            default: break;
        }
        return companyType;
    }

    /**
     * 获取文书对象的list中文书内容最长的索引
     *
     * @auther: yore
     * @param list 文书对象的list
     * @return 文书内容最长的索引
     * @date: 2018/10/10 11:30 AM
     */
    private static Integer getMaxBodyLenIndex(List<JSONObject> list){
        // 同一案号的多个裁判文书，取信息最全的
        int maxcpwsLen = 0, nowcpwsLen, maxCpwsIndex = 0;
        for(int i=0; i<list.size(); i++){
            nowcpwsLen = Util.removeBlankCaseNoAndJudegContent(list.get(i).getString("body")).length();
            if(nowcpwsLen>maxcpwsLen){
                maxcpwsLen = nowcpwsLen;
                maxCpwsIndex = i;
            }
        }
        return maxCpwsIndex;
    }

    /**
     * 生成裁判文书id
     *
     * @author:  yore
     * @param bodyStr 裁判文书原文
     * @return java.lang.String 文书MD5值
     * @date: 2018/8/20 18:20
     */
    public static String getCpwsId(String bodyStr){
        bodyStr = bodyStr.replace("<p>", "")
                .replace(" ","")
                .replace("　","")
                .replace("amp;","")
                .replaceAll("\\s*|\t*","")
                .replace("&#12295;","〇");
        bodyStr = StringUtils.replaceEach(bodyStr ,
                new String[]{"○","О","Ο","０","Ｏ","〇","零"},
                new String[]{"0","0","0","0","0","0","0"});
        return MD5Util.getMD5(bodyStr, (short)16);
    }

}
