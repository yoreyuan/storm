package com.yore.storm.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD5的工具类
 *
 * Created by yore on 2018-07-31 10:31
 */
public class MD5Util {
    // define a log object
    private static Logger LOG = LoggerFactory.getLogger(MD5Util.class);

    /**
     * 获取MD5值，
     * 返回32位小写
     *
     * @author: yore
     * @param text 字符串文本
     * @return java.lang.String
     * @date: 2018/7/31 3:48
     */
    public static String getMD5(String text ){
        Short bit = 32;
        return getMD5(text,bit);
    }
    /**
     * 获取MD5值，
     * 默认返回小写
     *
     * @author: yore
     * @param text 字符串文本
     * @param bit 16位或者32
     * @return java.lang.String
     * @date: 2018/7/31 3:48
     */
    public static String getMD5(String text, Short bit ){
        return getMD5(text,bit,false);
    }
    /**
     * 获取给定字符串的MD5值
     *
     * @author: yore
     * @param text 字符串文本,
     * @param bit 16位或者32,
     * @param isUpper 是否返回大写
     * @return java.lang.String
     * @date: 2018/7/31 9:24
     */
    public static String getMD5(String text, Short bit ,Boolean isUpper){
        if(bit.equals((short)16)){
            String md5Str = getMD5Bit16(text);
            return isUpper? md5Str.toUpperCase():md5Str;
        }else{
            String md5Str = getMD5Bit32(text);
            return isUpper? md5Str.toUpperCase():md5Str;
        }
    }


    /**
     * 获取给定字符串的MD5值(32位小写)
     *
     * @param text 字符串文本
     * @return java.lang.String MD5值
     * @date: 2018/7/31 9:24
     */
    public static String getMD5Bit32(String text){
        try {
            //获取 MessageDigest 对象，参数为 MD5 字符串，表示这是一个 MD5 算法（其他还有 SHA1 算法等）：
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            //update(byte[])方法，输入原数据
            //类似StringBuilder对象的append()方法，追加模式，属于一个累计更改的过程
            md5.update(text.getBytes("UTF-8"));
            //digest()被调用后,MessageDigest对象就被重置，即不能连续再次调用该方法计算原数据的MD5值。可以手动调用reset()方法重置输入源。
            //digest()返回值16位长度的哈希值，由byte[]承接
            byte[] md5Array = md5.digest();
            //byte[]通常我们会转化为十六进制的32位长度的字符串来使用,本工具类中还提供了另外三种常用的转换方法
            return bytesToHex1(md5Array);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException  e) {
            LOG.error("获取MD5值时发生了异常：{}",e.getMessage());
            return "";
        }
    }

    /**
     * 获取给定字符串的MD5值(32位大写)
     *
     * @author: yore
     * @param text 字符串文本
     * @return java.lang.String
     * @date: 2018/7/31 9:41
     */
    public static String getMD5Bit32UpperCase(String text){
        return getMD5Bit32(text).toUpperCase();
    }


    /**
     * 获取给定字符串的MD5值(16位小写)
     * 取标准32位MD5值的中间第9位到第24位的部分
     *
     * @param text 字符串文本
     * @return java.lang.String MD5值
     * @date: 2018/7/31 9:24
     */
    public static String getMD5Bit16(String text){
        return getMD5Bit32(text).substring(8,24);
    }

    /**
     * 获取给定字符串的MD5值(32位大写)
     *
     * @author: yore
     * @param text 字符串文本
     * @return java.lang.String
     * @date: 2018/7/31 9:41
     */
    public static String getMD5Bit16UpperCase(String text){
        return getMD5Bit16(text).toUpperCase();
    }


    private static String bytesToHex1(byte[] md5Array) {
        StringBuilder strBuilder = new StringBuilder();
        for (int i = 0; i < md5Array.length; i++) {
            int temp = 0xff & md5Array[i];
            String hexString = Integer.toHexString(temp);
            if (hexString.length() == 1) {
                //如果是十六进制的0f，默认只显示f，此时要补上0
                strBuilder.append("0").append(hexString);
            } else {
                strBuilder.append(hexString);
            }
        }
        return strBuilder.toString();
    }


    /**
     *
     * 通过java提供的BigInteger 完成byte->HexString
     * @param md5Array 字节数组
     * @return String
     */
    private static String bytesToHex2(byte[] md5Array) {
        BigInteger bigInt = new BigInteger(1, md5Array);
        return bigInt.toString(16);
    }


    /**
     *
     * 通过位运算 将字节数组到十六进制的转换
     * @param byteArray 字节数组
     * @return String
     */
    private static String bytesToHex3(byte[] byteArray) {
        char[] hexDigits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        char[] resultCharArray = new char[byteArray.length * 2];
        int index = 0;
        for (byte b : byteArray) {
            resultCharArray[index++] = hexDigits[b >>> 4 & 0xf];
            resultCharArray[index++] = hexDigits[b & 0xf];
        }
        return new String(resultCharArray);
    }

}