package com.yore.storm;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.elasticsearch.bolt.EsIndexBolt;
import org.apache.storm.elasticsearch.common.DefaultEsTupleMapper;
import org.apache.storm.elasticsearch.common.EsConfig;
import org.apache.storm.elasticsearch.common.EsTupleMapper;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.kafka.spout.KafkaSpout;
import org.apache.storm.kafka.spout.KafkaSpoutConfig;
import org.apache.storm.topology.TopologyBuilder;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 *
 *
 * Created by yore on 2018/11/19 14:00
 */
@Slf4j
public class TopologyEntry {

    public static void main(String[] args) {

        // Topology Config
        Properties topologyConfig = new Properties();
        try {
            topologyConfig.load(TopologyEntry.class.getResourceAsStream("/topology.properties"));
        } catch (IOException e) {
            log.error("Storm拓扑初始化时，加载配置对象时发生了异常: {}",e.getMessage());
        }

        // kafka server的基本配置
        Properties properties = new Properties();
        properties.setProperty("group.id", topologyConfig.getProperty("kafka.group.id"));
        // 定义一个KafkaSpoutConfig
        KafkaSpoutConfig<String, String> kafkaSpoutConfig = KafkaSpoutConfig
                .builder(topologyConfig.getProperty("kafka.bootstrap.servers"),
                        topologyConfig.getProperty("kafka.source.topic"))
                .setFirstPollOffsetStrategy(KafkaSpoutConfig.FirstPollOffsetStrategy.UNCOMMITTED_EARLIEST)
                .setProp(properties).build();
        // KafkaSpout实现
        KafkaSpout<String, String> kafkaSpout = new KafkaSpout<>(kafkaSpoutConfig);


        // ES Bolt
        EsConfig esConfig = new EsConfig(new String[]{
                "http://i-ef32owrg:9200",
                "http://hbm8wksw:9200",
                "http://i-yxzzcusk:9200"
        });
        Map<String, String> additionalParameters = new HashMap<>();
        additionalParameters.put("client.transport.sniff", "true");
//        esConfig(ContentType.APPLICATION_JSON)
        // 定义ES默认的映射
        EsTupleMapper esTupleMapper = new DefaultEsTupleMapper();
        // 定义一个索引Bolt
        EsIndexBolt indexBolt = new EsIndexBolt(esConfig, esTupleMapper);

        //build Storm Topology
        TopologyBuilder builder = new TopologyBuilder();
        builder.setSpout("kafka-spourt", kafkaSpout, 1);
        builder.setBolt("retrieve-bolt", new RetrieveBolt(), 1).localOrShuffleGrouping("kafka-spourt");
        builder.setBolt("es-bolt", indexBolt, 1)
                .localOrShuffleGrouping("retrieve-bolt");

        //Create a Storm Cluster Config Obj
        Config config = new Config();
        config.setNumWorkers(Integer.parseInt(topologyConfig.getProperty("topology.workers")));
        config.setMessageTimeoutSecs(Integer.parseInt(topologyConfig.getProperty("topology.message.timeout.secs")));
        config.setMaxSpoutPending(Integer.parseInt(topologyConfig.getProperty("topology.max.spout.pending")));
        config.setDebug(Boolean.parseBoolean(topologyConfig.getProperty("topology.debug")));

        // is run Strom Tomplogy in Local
        boolean isLocal = Boolean.parseBoolean(topologyConfig.getProperty("topology.deplogy.local"));
        System.out.println("$$$\t" + isLocal);
        if(isLocal){
            // local
            LocalCluster cluster = new LocalCluster();
            // Submit Topology
            cluster.submitTopology(topologyConfig.getProperty("topology.name"),config,builder.createTopology());
        }else {
            try {
                String topologName = args.length==0|| StringUtils.isEmpty(args[0])? topologyConfig.getProperty("topology.name"):args[0];
                //Storm cluster
                StormSubmitter.submitTopology(topologName,config,builder.createTopology());
            } catch (AlreadyAliveException | InvalidTopologyException | AuthorizationException e) {
                e.printStackTrace();
                log.error("Storm提交拓扑时发生了异常: {}",e.getMessage());
            }
        }

    }

}
