package com.yore.storm;

import com.alibaba.fastjson.JSONObject;
import com.yore.storm.utils.Util;
import org.apache.commons.lang.StringUtils;
import org.apache.http.Consts;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import java.util.List;

/**
 * Created by yore on 2018/11/19 15:43
 */
public class RetrieveBolt extends BaseBasicBolt {

    @Override
    public void execute(Tuple input, BasicOutputCollector collector) {
        // 获取流中的数据
        String jsonDataStr = input.getStringByField("value");

        // 判断接收的数据是否为空,如果为空直接返回
        if(StringUtils.isBlank(jsonDataStr))return;

        // 字符串转JSON对象，并判断如果结果为null本次不执行
        JSONObject jsonObject = Util.getParseJson(jsonDataStr);
        // 判断解析成的JSON对象是否为空，若为空直接返回
        if (jsonObject == null || null == jsonObject.getJSONArray("borrowerList")) return;

        String termVersion = jsonObject.getString("version");
        // 没有周期数标识的数据不进行处理
        if(StringUtils.isBlank(termVersion))return;

        String relater_cert_no = jsonObject.getString("relater_cert_no");

        for(Object o : jsonObject.getJSONArray("borrowerList")){
            JSONObject borrowerJsonObj = (JSONObject)o;

            /* ---------- 个人裁判文书分析---------- */
            List<JSONObject> persionCpwsList = Util.getCpwsJsonObjList(jsonObject,1);
            dealCpwsFun(borrowerJsonObj.getString("borrower_cert_no"), persionCpwsList, collector);

            /* ---------- 企业接口数据, type=3为带有类型的---------- */
            List<JSONObject> companyCpwsList = Util.getCpwsJsonObjList(jsonObject,3);
            dealCpwsFun(borrowerJsonObj.getString("borrower_cert_no"), companyCpwsList, collector);

        }


    }


    /**
     * 我们常常将Elasticsearch的index比作关系型数据库的database、Type比作table，
     * 但是这样比喻是没有任何意义的
     *
     * Index
     *    Index存储在多个分片中，其中每一个分批拿都是一个独立的Lucence Index。
     *    因此，新添加index应该是有个限度，每个Lucene Index都需要消耗一些磁盘、内存和文件描述符。
     *    一个大的index比一个小的index效率更高。
     *
     * Type
     *    一个Type就是一个Index的一种类型的数据，可以使用这个减少index的数量。
     *    在使用时，向每个文档加入_type字段，在指定type搜索时就会被用于过滤。
     *    使用Type的一个好书就是，搜索一个index的多个type，和搜索一个type相比没有额外的开销
     *
     *    使用Type的限制
     *       ①不同的Type里的字段需要保持一致。
     *       ②只在某个Type里存的字段，在其他Type中没有该字段，也会消耗资源
     *       ③得分是由Index内的统计数据来决定的。也就是一个type的数据会影响另一个type的文档的得分。
     *
     * @auther: yore
     * @date: 2018/11/20 9:56 AM
     */
    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        // 定义消息发送的字段映射，这里是EsTupleMapper所需要的字段映射逻辑，可跟踪源代码理解
        declarer.declare(new Fields("source", "index", "type", "id"));
    }


    private void dealCpwsFun(String type, List<JSONObject> cpwsList, BasicOutputCollector collector){
        for(JSONObject cpwsJsonObj : cpwsList){
            // 文书主体内容
            String j_body = cpwsJsonObj.getString("body");
            // 企业类型
            String j_type = cpwsJsonObj.getString("type");

            String case_type = StringUtils.isBlank(j_type)? "11" : Util.getCompanyType(j_type);
            String id = Util.getCpwsId(case_type + j_body);

            System.out.println("...\t"+id);

            JSONObject sourceJson = new JSONObject();
            sourceJson.put("judge_content", j_body);

            /**
             * 查看EsIndexBolt源码，默认发送数据时client传入的参数为
             * client.performRequest("put", getEndpoint(index, type, id), params, new StringEntity(source), new Header[0]);
             * 其中new StringEntity(source),构造时设定的sorce类型为ContentType.DEFAULT_TEXT
             * 而ContentType.DEFAULT_TEXT使用的是TEXT_PLAIN类型，
             * TEXT_PLAIN设置的编码格式为Consts.ISO_8859_1
             *
             * @auther: yore
             * @date: 2018/11/21 2:12 PM
             */
            String ss = new String(sourceJson.toJSONString().getBytes(), Consts.ISO_8859_1);
            System.out.println(ss);
            collector.emit(new Values(ss, "m_cpws_info", type, id));

        }

    }

}
